// ROOT includes
#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TF1.h"
#include "TString.h"
#include "TMath.h"
#include "TVirtualFFT.h"
#include "TApplication.h"
#include "TCanvas.h"

// Standard includes
#include <vector>
#include <string>
#include <fstream>
#include <iomanip>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sstream>
#include <stdlib.h>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <dirent.h>
#include <algorithm>
#include <chrono>
#ifndef __has_include
  static_assert(false, "__has_include not supported");
  #else
  #  if __cplusplus >= 201703L && __has_include(<filesystem>)
  #    include <filesystem>
       namespace fs = std::filesystem;
  #  elif __cplusplus < 201703L &&__has_include(<filesystem>)
  #    include <filesystem>
       namespace fs = std::__fs::filesystem;
  #  elif __has_include(<experimental/filesystem>)
  #    include <experimental/filesystem>
       namespace fs = std::experimental::filesystem;
  #  elif __has_include(<boost/filesystem.hpp>)
  #    include <boost/filesystem.hpp>
       namespace fs = boost::filesystem;
  #  endif
#endif


// Lorenzo includes
#include "Setup.h"
#include "General_Functions.h"
#include "Conversion.h"
#include "Wave_Analyzer.h"


using namespace std;
//namespace fs = std::filesystem;

int main(int argc, char** argv){
  
  //#######################  HANDLING COMMAND  #######################
  
  

  ifstream InputCARD;
  ifstream runlist;
  ifstream runlist_temp_check;
  bool list = false;
  bool corrupted = false;
  
  if(cmdOptionExists(argv, argv+argc, "-h")){
    std::cout << "Usage: " << argv[0] << " RUNLIST.csv [-c CARD]" << std::endl << "OR" << endl;
    std::cout << "Usage: " << argv[0] << " PATH_TO_FOLDER [-o FILE_NAME.root] [-c CARD]" << std::endl;
    std::cout << endl << "A template for the RunList is provided in RunList_template.csv. To generate the RunList_template run the Analysis script w/o arguments." << endl; 
    std::cout << endl << "List of available options:" << endl;
    std::cout << "-c CARD:           \tExplicitly pass an input card." << endl << endl;
    std::cout << "-o FILE_NAME.root: \tDefines explicit name for output file. Cannot be used with a RunList." << endl << endl;
    return(EXIT_SUCCESS);
  }

  runlist_temp_check.open("RunList_template.csv");
  if(!runlist_temp_check){
    std::ofstream runlist_temp("RunList_template.csv", std::ofstream::out);
    runlist_temp << "Input_folder,Output_file,FirstSegment(optional),LastSegment(optional)" << endl;
    runlist_temp << "/path/to/data/folder1,path/to/output/file1,FirstSegment,LastSegment" << endl;
    runlist_temp << "/path/to/data/folder2,path/to/output/file2,FirstSegment,LastSegment" << endl;
  }
  if(argc>=2){
    if(fs::exists(argv[1]) && fs::is_directory(argv[1])){
      cout << "Converting data in folder: " << argv[1] << endl;
      list = false;
    }
    else{
      runlist.open(argv[1]);
      if(!runlist){
	      std::cerr << "Could not find the RunList.";
        std::cerr << argv[0] << " -h for usage and detailed list of options." << endl;
	      exit(EXIT_FAILURE);
      }
      else{
	      list = true;
	      cout << "RunList: " << argv[1] << endl;
      }
    }
  }
  else{
    std::cerr << argv[0] << " -h for usage and detailed list of options." << endl;
    exit(EXIT_FAILURE);
  }


  //#######################   DATA I/O AND PARAMETER SETUP  #######################


  // Default parameters and initializations

  bool debug[16] = {}; // flag to debug channels
  int nchro=16; // maximum number of input readout channels
  int nchout=16; // maximum number of output analysis channels
  bool doFFT[16] = {};  // flag to decide if doing the FFT.
  int showFFT[16] = {}; // flag to decide if showing the FFT in the output file.
  bool domobileavg[16] = {}; // flag to decide if doing the mobile average
  string lowpass[16];

  double fcuthigh[16]; // low-pass frequency cut with mobile average in Hz
  std::fill_n(fcuthigh, 16, 100e9); 
  bool highpass[16] = {}; // flag to decide if doing a highpass filter
  double fcutlow[16]; // low-pass frequency cut with FFT selection expressed in Hertz
  std::fill_n(fcutlow, 16, 0.02);
  int showwave[16] = {};  // flag to decide if storing the waveform in the output file
  bool printheader=false; // flag to decide if printing the header on screen
  bool stdvariables[16]; // flag to decide if storing standard analysis variables
  std::fill_n(stdvariables, 16, 1);
  bool doshift[16] = {}; // flag to decide if going a waveform shift and subtraction
  Double_t shift[16] = {}; // time shift in ns
  int campfact[16];  //divider to sample rate for waveform memorization
  std::fill_n(campfact, 16, 1);
  int maxtrig=999999;
  Double_t t_bck_low[16]; // lower limite for background search in ns
  std::fill_n(t_bck_low, 16, 1);
  Double_t t_bck_high[16]; // upper limit for background search in ns
  std::fill_n(t_bck_high, 16, 10);
  Double_t t_pul_low[16]; // lower limit for pulse search in ns
  std::fill_n(t_pul_low, 16, 10); 
  Double_t t_pul_high[16]; // upper limit for pulse search in ns
  std::fill_n(t_pul_high, 16, 500); 
  int nthr[16] = {}; // number of active thresholds per channel
  Double_t thr[16][100]; // thresholds to be used for the analysis in mV. Up to 100 thresholds per channel can be defined
  for(int ch = 0;ch<16;ch++){
    std::fill_n(thr[ch], 100, 20); 
  }
  int polarity[16]; // polarity of the input signal, needed for the analysis
  std::fill_n(polarity, 16, 1); 
  string outdef[16] = {"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16"}; // defines the standard inputs to be used for the output analysis channels

  // Program variables

  std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
  std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
  string selection="";
  bool newcard=false;
  string pip;
  bool running=true;
  string inpath[1000];
  string outpath[1000];
  string firstseg[1000];
  string lastseg[1000];
  int nruns=0;
  vector<string> runmatrix[16]; //temporary matrix where the names of the data files are stored
  int runindex[16];
  double SR; // sampling rate (in Gs/s) extracted from waveform
  double alpha; // attenuation factor for exponential mobile average
  Double_t doublebuffer;
  Double_t doublebuffer2;
        
  // Opening Input parameter CARD

  bool askforcard = true;
  if(cmdOptionExists(argv, argv+argc, "-c")){
    string cardname = get1stCmdOption(argv, argv + argc, "-c");
    InputCARD.open(cardname);
    askforcard = false;
    cout << "Using input card in path: " << cardname << endl;
    if(!InputCARD){
      std::cerr << "Could not find the input card. Exiting program." << endl;
      return(EXIT_FAILURE);
    }
  }
  else{
    InputCARD.open("./InputCARD.in");
  }
  cout << endl;
  
  // Reading/building new InputCARD
  CardSETUP( &InputCARD, &nchro, &nchout, &outdef[0], &doFFT[0], &showFFT[0], &domobileavg[0], &lowpass[0], &fcuthigh[0], &fcutlow[0], &showwave[0], &printheader, &stdvariables[0], &doshift[0], &shift[0], &campfact[0], &t_bck_low[0], &t_bck_high[0], &t_pul_low[0], &t_pul_high[0], &nthr[0], thr, &polarity[0], askforcard, &debug[0]);
  // Reading RunList
  if(list){
    ReadList( &runlist, &inpath[0], &outpath[0],&firstseg[0], &lastseg[0], &nruns);
    cout << endl << nruns << " runs will be analyzed." << endl;
  }
  else{
    nruns = 1;
    inpath[0] = argv[1];
    string tempstr = argv[1];
    if(cmdOptionExists(argv, argv+argc, "-o")){
      outpath[0] = get1stCmdOption(argv, argv + argc, "-o");
      if(outpath[0].substr(outpath[0].length()-5,outpath[0].length())!=".root"){
        std::cerr << "The output file should be .root format. Exiting program." << endl;
        return(EXIT_FAILURE);
      }
    }
    else{
      if(tempstr.compare(tempstr.length()-1,1,"/") == 0){
        outpath[0] = tempstr.substr(0,tempstr.length()-1) + ".root";
      }
      else{
        outpath[0] = tempstr.substr(0,tempstr.length()) + ".root";
      }
    }
    cout << endl << endl << "Outputfile: " << outpath[0] << endl;
  }

  //TApplication App("app", &argc, argv);
  selection="";

  //#######################   RUN LOOP  #######################

  for(int n=0;n<nruns;n++){

    //#######################   VARIABLE AND OUTPUT FILE DEFINITION  #######################

    // Crating output root file
    TFile *OutputFile = new TFile(outpath[n].c_str(), "RECREATE");
    OutputFile->cd();
    //ofstream ofs ("test.txt", std::ofstream::out);
    //ofs << "lorem ipsum";
    //ofs.close();
    
    uint nfirstseg = 0;
    uint nlastseg = 1000000;

    if(is_number(firstseg[n]) || is_number(lastseg[n])){
      cout << "Selected segment range for run " << n+1 << ":" << endl;
    }
    if(is_number(firstseg[n])){
      nfirstseg=stol(firstseg[n]);
      cout << "First Segment = " << nfirstseg << endl;
    }
    if(is_number(lastseg[n])){
      nlastseg=stol(lastseg[n]);
      cout << "Last Segment = " << nlastseg << endl;
    }
    if(nfirstseg>nlastseg){
      cerr << "ERROR: first segment should be smaller than last segment." << endl;
      return(EXIT_FAILURE);
    }
    
    for(int i=0;i<8;i++){
      runmatrix[i].clear();
    }

    // Building runmatrix from runlist
    int matx=RunMatrix( n, nfirstseg, nlastseg, &runindex[0], runmatrix, &inpath[0], &outpath[0]);
    if(matx==0){
      return(EXIT_FAILURE);
    }

    // Scan variables
    Int_t nrun = n+1; // Run position in the RunList
    Int_t ntrig = 0; // Event number in the run
    Int_t nseg = 0; // Segment number
    int SegTrig = 0; // Counter of triggers inside a segment


    
    // Waveform variables
    Double_t year; // (6 variables) Wavefo rm acquisition date
    Double_t month; 
    Double_t day; 
    Double_t hour;
    Double_t mnt;
    Double_t sec;
    double TrigTime; // Absolute trigger time
    Int_t chan[nchro]; // Channel identifier
    Int_t Nbck[nchro]; // Number of background samples
    Double_t bck[nchout]; // Average background value
    Double_t max_bck[nchout]; // Maximum background value
    Double_t max_time_bck[nchout]; // waveform time at maximum of background
    Double_t rms_bck[nchout]; // RMS of background

    Int_t camp[nchout]; // Number of waveform samples

    const Int_t max_camp = 48100; // Maximum number of samples allowed for the waveform
    const Int_t max_FFT_camp = 48100; // Maximum number of samples allowed for the FFT
  
    vector<double_t> time[nchro]; // Input time vectors
    vector<double_t> amp[nchro]; // Input amplitude vectors
    vector<double_t> m_time[nchout]; // Processed time vectors
    vector<double_t> m_amp[nchout]; // Processed amplitude vectors

    vector<double_t> freq[nchout]; // Frequency vectors
    vector<double_t> FFT_real[nchout]; // Real component of FFT vectors
    vector<double_t> FFT_imag[nchout]; // Imaginary component of FFT vectors
    vector<double_t> FFT_abs[nchout]; // Absolute value of FFT vectors
    
    
    Double_t max[nchout]; // Maximum of largest pulse
    Double_t amplitude[nchout]; // Amplitude of largest pulse
    Double_t min[nchout]; // Waveform lowest point
    Double_t max_time[nchout]; // waveform time at maximum
    Double_t amp_time[nchout]; // Largest pulse peaking time
    Double_t time_10const[nchout]; // Time at 10% of the amplitude
    Double_t time_20const[nchout]; // Time at 20% of the amplitude
    Double_t time_50const[nchout]; // Time at 50% of the amplitude
    Double_t time_80const[nchout]; // Time at 80% of the amplitude
    Double_t time_90const[nchout]; // Time at 90% of the amplitude
    Double_t time_3rms[nchout]; // Rising edge time at 3rms of the background
    Double_t time_5rms[nchout]; // Rising edge time at 5rms of the background
    Double_t time_thr[nchout][100]; // Rising edge time at custom fixed thresholds
    Double_t trail_thr[nchout][100]; // Falling edge time at 1mV fixed threshold
    Double_t pulsecharge[nchout]; // Charge in the pulse window
    
    // Tree definition
    TTree *EventData = new TTree("EventData","EventData");
    EventData->SetDirectory(OutputFile);

    
    // Branches definition
    GeneralBranchDefinition(EventData, &ntrig, &SegTrig, &nseg, &nrun, &nchout, &year, &month, &day, &hour, &mnt, &sec, &TrigTime);
    for(int chout=0;chout<nchout;chout++){
      BaseBranchDefinition(EventData, chout, &t_bck_low[chout], &t_bck_high[chout], &bck[chout], &max_bck[chout], &rms_bck[chout], &max_time[chout], &max_time_bck[chout], &max[chout],  &pulsecharge[chout]);
      for(int thrid=0;thrid<nthr[chout];thrid++){
        ThrBranchDefinition(EventData,chout,thr[chout][thrid],&time_thr[chout][thrid],&trail_thr[chout][thrid]);
      }
      if(stdvariables[chout]){
        StandardBranchDefinition(EventData, chout, &time_10const[chout], &time_20const[chout], &time_50const[chout], &time_80const[chout], &time_90const[chout], &time_3rms[chout], &time_5rms[chout]);
      }
      if(showwave[chout]!=0){
        RecordBranchDefinition(EventData, chout, &m_time[chout], &m_amp[chout]);
      }
      if(doFFT[chout] && showFFT[chout]>0){
        FFTBranchDefinition(EventData, chout, &freq[chout], &FFT_real[chout], &FFT_imag[chout], &FFT_abs[chout]);
      }
    }
  
    // Program variables
    string datafile;
    int emptycount=0;
    bool bad=false;
    bool header_printed=false;
    int arrayoff;
    uint datapos;
    int SegCount = 0;
    Double_t chi2;
    bool setns=true;
    Int_t ns=1;
    vector<char> datadump[nchro];
    //const char *datamap;
    bool dumprun = false;
    double dynamicthr[nchro];
    bool dynamicthrrun[nchro];
    uint nevents;
        
    //Inizialization of data run variables
    ntrig=0;
    nseg=0;

    //Inizialization of program run variables
    bool active_run=true;
    pip = "";
    


    cout << endl << endl << "############################################### " << endl << endl;
    cout << "Run number: " << nrun << endl;
    cout << "Number of Segments = " << runindex[0] << endl;
    cout << "Run data path: " << inpath[n] << endl;
    
    for(int chin=0;chin<nchro;chin++){
      time[chin].clear();
      amp[chin].clear();
      camp[chin]=0;
    }

    //Loop on different triggers of the run
    //cout << "\nEvent number: " << flush;
    ifstream data[8];
    int byteoff = 0;
    int HeaderLength = 0;
    int FirstValidPoint = 0;
    float HorizontalInterval = 0;
    float VerticalGain = 0;
    float VerticalOffset = 0;
    int dataformat;
    int lasttrigrate = 0;
    double avgrate = 0;
    begin = std::chrono::steady_clock::now(); // measuring program speed
    while(active_run){
      //cout << "Entering active run " << endl; //debug
      ntrig++;
      SegTrig++;
      bad=false;
      avgrate = 0;
      bool showwave_select = false;
      // Check if the segment is finished
      if(SegTrig==SegCount+1){
        nseg++;
        header_printed=false;
        byteoff = 0;
        HeaderLength = 0;
        FirstValidPoint = 0;
        HorizontalInterval = 0;
        VerticalGain = 0;
        VerticalOffset = 0;
        dataformat=-1;
        SegTrig=1;
        for(int chin=0;chin<nchro;chin++){
	        //#######################   OPENING DATA FILES  #######################
	        datafile="";
	        datafile=runmatrix[chin].at(nseg-1);  //Read file containing the event on that channel
          if(data[chin].is_open()){
            data[chin].close();
          }
	        data[chin].open(datafile.c_str(), ios::binary|ios::ate);
          //cout << i << " file size " << data[chin].tellg() << endl;
	        if(!data[chin]){            
	          active_run=false;
	          bad=true;
	          cerr << "\nData file not found! Run " << nrun << "  Event " << ntrig << endl << flush;
	          cout << chin << "\t" << nseg << endl;
	          cout << runmatrix[chin].at(nseg-2) << endl;
	          cout << "End of run" << endl << flush;
	          return(EXIT_FAILURE);
	        }
          /*
          if((data[chin].tellg()<200000000 && 0)){
            int fd = open(datafile.c_str(), O_RDONLY);
            cout << endl << "Data size: " << data[chin].tellg() << endl;
            const char* datamap = (const char*)mmap(NULL,1000,PROT_READ|PROT_WRITE,MAP_PRIVATE,fd,0);
            if(datamap == MAP_FAILED){
              printf("Mapping Failed\n");
            return (EXIT_FAILURE);
            }
            cout << "map size = " << strlen(datamap) << endl;
            close(fd);
            dumprun=true;
            if(i==0){
              cout << endl << "Dumping data in RAM " << endl;
            }
          }
          */
	      }
      }
      // Check if the run is finished
      if(nseg == runindex[0] && SegTrig==SegCount){
        active_run = false;
      }
      if(ntrig==maxtrig){
	      cout << "Maximum number of events reached: " << maxtrig << endl << flush;
	      active_run=false;
      }
      if(ntrig==2 || (int(100000*double(ntrig)/double(nevents))%2000==0 && ntrig>2)){
        end = std::chrono::steady_clock::now();
        double evrate = 1000.*(double(ntrig))/std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
        int timetocomp = int(10000.*double(nevents-ntrig)/double(10000.*evrate));
        printf("\r%2d %% completed. Processing %3.1f events per second. %3dh %2dm %2ds to completion.",int(100*double(ntrig)/(runindex[0]*SegCount)),evrate,timetocomp/3600,(timetocomp/60)%60,timetocomp%60);
        fflush(stdout);
        lasttrigrate = ntrig;

      	//cout << ntrig << ", " << flush;
      }
      

      //#######################   READING CHANNELS  #######################
      for(int chin=0;chin<nchro;chin++){
        // Reading the header
        HeaderRO(&data[chin], printheader, SegTrig, max_camp, &SegCount, &active_run, &bad, &datapos, &header_printed, &camp[chin], &FirstValidPoint, &HorizontalInterval, &VerticalGain, &VerticalOffset, &dataformat, &day, &month, &year, &hour, &mnt, &sec, &TrigTime, false);

        if(chin==0 && nseg==1 && SegTrig==1){
          nevents = runindex[0]*SegCount;
          cout << endl << "Number of Events = " << nevents << endl << endl;
        }
        time[chin].clear();
        amp[chin].clear();
        while(time[chin].size()<camp[chin]){ // it used to be <=
          time[chin].push_back(0);
        }
        while(amp[chin].size()<camp[chin]){
          amp[chin].push_back(0);
        }
        

        

        if(datapos<0){
          cerr << "ERROR: invalid position on file readout." << endl;
          return(EXIT_FAILURE);
        }
        // reading waveform      
        //cout << "Reading waveform" << endl;
        EventRO(&data[chin], datapos, camp[chin], SegTrig, time[chin], amp[chin], dataformat, FirstValidPoint, HorizontalInterval, VerticalGain, VerticalOffset);
      }

      //#######################   PROCESSING INPUT  #######################
      
      for(int chout=0; chout<nchout;chout++){
        if(debug[chout]){
          cout << "processing channel " << chout+1 << endl;
        }
        m_time[chout].clear();
        m_amp[chout].clear();
        //cout << "Vectors cleared" << endl; //debug
        bool opdiff = false;
        bool opsum = false;
        int chin = 0; // reference input channel for vector size
        int chop1 = 0;
        int chop2 = 0;
        // Is it an operation on channels?
        if(outdef[chout].find('-') == string::npos && outdef[chout].find('+')==string::npos){
          // no operation between waveforms
          //cout << "No operation for this output" << endl; //debug
          chin = stoi(outdef[chout]) - 1;
          m_time[chout] = time[chin];
          m_amp[chout] = amp[chin];
        }
        else if(outdef[chout].find('-')<outdef[chout].length()){
          // it is operation on channels
          //cout << "Computing wave difference" << endl; //debug
          opdiff = true;
          chop1 = stoi(outdef[chout].substr(0,outdef[chout].find('-'))) - 1;
          chop2 = stoi(outdef[chout].substr(outdef[chout].find('-')+1,outdef[chout].length())) - 1;
          chin = chop1;
          m_time[chout] = time[chin];
          waveoperation(amp[chop1],amp[chop2],&m_amp[chout],"-"); 
        }
        else if(outdef[chout].find('+')<outdef[chout].length()){
          // it is operation on channels
          opsum = true;
          chop1 = stoi(outdef[chout].substr(0,outdef[chout].find('+'))) - 1;
          chop2 = stoi(outdef[chout].substr(outdef[chout].find('+')+1,outdef[chout].length())) - 1;
          chin = chop1;
          m_time[chout] = time[chin];
          waveoperation(amp[chop1],amp[chop2],&m_amp[chout],"+");
        }
        else{ 
          std::cerr << "Unexpected output definition, exiting program..." << endl;
          return(EXIT_FAILURE);
        }
        if(polarity[chout]<0){
          inversion(&m_amp[chout]);
        }
        //cout << "Array building complete" << endl; //debug
        if(m_time[chout].at(m_time[chout].size()-1)<t_bck_low[chout]){
          cerr << "ERROR: Upper limit for background should be lower than waveform length ("<< m_time[chout].at(m_time[chout].size()-1) <<" ns). Please, correct input CARD." << endl << "Exiting program." << endl;
          return(EXIT_FAILURE);
        }
        
        if(doshift[chout]){
          int nskew = shift[chout]/(m_time[chout].at(1)-m_time[chout].at(0));
          pulseskew(m_amp[chout], nskew, &m_amp[chout]);
        }

        if(domobileavg[chout]){
          if(lowpass[chout] == "lin"){
            setns = true;
            ns = 1; // initial value of ns
            while(setns){
              SR=(1.e9/(m_time[chout][1]-m_time[chout][0])); // sampling rate
              //cout << "fcuthigh = " << fcuthigh[chout] << " SR = " << SR << endl; 
              double Hf=(1./double(ns))*fabs(sin(3.14*fcuthigh[chout]*double(ns)/SR)/sin(3.14*fcuthigh[chout]/SR)); // Attenuation at cutoff frequency
              if(Hf<0.87){
                setns=false;
                //cout << "Doing mobile average with " << ns << " poins, at a sampling rate of " << (SR*1e-9) << " GS/s." << endl;
                //cout << "Attenuation at cutoff frequency:" << Hf << endl;
              }
              else{
                ns=ns+2;
              }
              if(3*fcuthigh[chout]>SR){
                ns=1;
                setns=false;
              }
              if(ns>m_time[chout].size()/10){
                //cout << endl << endl << "WARNING: The cutoff frequency is too low. No mobile average will be applied." << endl << endl;
                ns=1;
                setns=false;
              }
            }
            if(ns>1){
              mobileAVG(m_amp[chout], ns, &m_amp[chout]);
            }
          }
          else if(lowpass[chout] == "exp"){
            alpha = (2*4*atan(1)*(m_time[chout][1]-m_time[chout][0])*1e-9*fcuthigh[chout])/(2.*4.*atan(1)*(m_time[chout][1]-m_time[chout][0])*1e-9*fcuthigh[chout] +1.);
            double attcut = 1e-2;
            int ncut = 0;
            while(ncut<1000 && alpha*pow((1-alpha),ncut)>attcut){
              ncut++;
            }
            expo_mobileAVG(m_amp[chout], ncut, alpha, &m_amp[chout]);
          }
          if(debug[chout]){
            cout << "finished doing mobile avg" << endl;
          }
          
        }

        // Requires debugging!
        if(doFFT[chout] && ntrig<showFFT[chout]){   
          freq[chout].clear();
          for(int l=0; l<m_amp[chout].size(); l++){
            freq[chout].push_back(double(l)/(HorizontalInterval*m_time[chout].size())*1e-9);
          }
          if(debug[chout]){
            cout << "doing FFT " << endl;
          }
          // Generating FFT
          FFTrealtocomplex(m_amp[chout], &FFT_real[chout], &FFT_imag[chout], &FFT_abs[chout]);

          // ### FFT OPERATIONS ####
          if(highpass[chout]){
            HighPass(freq[chout], &FFT_real[chout],&FFT_imag[chout],fcutlow[chout]);
          }
          // Regenerating waveform from the FFT
          FFTcomplextoreal(FFT_real[chout], FFT_imag[chout], &m_amp[chout]);
          if(debug[chout]){
            cout << "finished FFT " << endl;
          }
        }
        if(ntrig<showwave[chout]){
          showwave_select = true;
        }
        //cout << "waveform processed" << endl; //debug
      }
      
      //#######################   WAVEFORM ANALYSIS  #######################
      for(int chout=0;chout<nchout;chout++){
        if(debug[chout]){
          cout << "starting analysis on channel " << chout+1 << endl; //debug
        }
        // Background analysis
        // NOTE: ALWAYS RUN BACKGROUND FUNCTION BEFORE RUNNING FURTHER ANALYSIS
	      Background(m_time[chout], m_amp[chout], t_bck_low[chout], t_bck_high[chout], &bck[chout], &max_bck[chout], &rms_bck[chout], &max_time_bck[chout]);
        // Amplitude analysis
        Amplitudes(m_time[chout], m_amp[chout], bck[chout], rms_bck[chout], t_pul_low[chout],t_pul_high[chout], &max[chout], &max_time[chout], &amplitude[chout], &amp_time[chout], &chi2);
        Charges(m_time[chout], m_amp[chout], t_pul_low[chout], t_pul_high[chout], bck[chout], &pulsecharge[chout], &doublebuffer, 50);
        if(chi2>2){
          amplitude[chout]=max[chout];
        }

        // Time variables
        Thrtime(m_time[chout], m_amp[chout], t_pul_low[chout], t_pul_high[chout], bck[chout], thr[chout], time_thr[chout],nthr[chout],max[chout]);
        Trailtime(m_time[chout], m_amp[chout], bck[chout], &thr[chout][0], time_thr[chout], trail_thr[chout],nthr[chout],0.15); // default hysteresys at 0.15 ns
        if(stdvariables[chout]){
          double stdthr1[1] = {3*rms_bck[chout]};
          Thrtime(m_time[chout], m_amp[chout], t_pul_low[chout], t_pul_high[chout], bck[chout], &stdthr1[0], &time_3rms[0], 1, max[chout]);
          double stdthr2[1] = {5*rms_bck[chout]};
          Thrtime(m_time[chout], m_amp[chout], t_pul_low[chout], t_pul_high[chout], bck[chout], &stdthr2[0], &time_5rms[0], 1, max[chout]);
          ConstFracTime(m_time[chout], m_amp[chout], t_pul_low[chout], t_pul_high[chout], bck[chout], max_time[chout], max[chout]*0.1, &time_10const[chout]);
          ConstFracTime(m_time[chout], m_amp[chout], t_pul_low[chout], t_pul_high[chout], bck[chout], max_time[chout], max[chout]*0.2, &time_20const[chout]);
          ConstFracTime(m_time[chout], m_amp[chout], t_pul_low[chout], t_pul_high[chout], bck[chout], max_time[chout], max[chout]*0.5, &time_50const[chout]);
          ConstFracTime(m_time[chout], m_amp[chout], t_pul_low[chout], t_pul_high[chout], bck[chout], max_time[chout], max[chout]*0.8, &time_80const[chout]);
          ConstFracTime(m_time[chout], m_amp[chout], t_pul_low[chout], t_pul_high[chout], bck[chout], max_time[chout], max[chout]*0.9, &time_90const[chout]);
        }
        
      } // Loop on channels ends
      
      //Place your multichannel analysis here!

      for(int chout = 0; chout< nchout; chout++){
        if(ntrig>showwave[chout]){
          m_amp[chout].clear();
          m_time[chout].clear();
        }
        if(ntrig>showFFT[chout]){
          freq[chout].clear();
          FFT_real[chout].clear();
          FFT_imag[chout].clear();
          FFT_abs[chout].clear();
        }
      }

      if(bad)
	      cout << "Event number " << ntrig << " is bad!" << endl;
      
      // Filling the event in the tree
      if(!bad){
        EventData->Fill();
      }

      // Writing on the output file
      if(!showwave_select && ntrig%5000==0){
        OutputFile->cd();
        EventData->Write();
      }
      if(showwave_select && ntrig%500==0){
        OutputFile->cd();
        EventData->Write();
      }
      
    } // Run loop ends here
    OutputFile->cd();
    EventData->Write();
    OutputFile->Close();
  } // Scan loop ends here
  cout << endl << endl << "Waveform conversion and analysis completed successfully" << endl;
  return(EXIT_SUCCESS);
}
