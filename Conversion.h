#ifndef CONVERSION_H
#define CONVERSION_H

#include <iostream>
#include <string.h>
#include <iomanip>
#include <stdio.h>

using namespace std;

//Convert little endian int to big endian int
unsigned int intToBigEndianess(const unsigned int x)
{
  return  ( x >> 24 ) |  // Move first byte to the end,
          ( ( x << 8 ) & 0x00FF0000 ) | // move 2nd byte to 3rd,
          ( ( x >> 8 ) & 0x0000FF00 ) | // move 3rd byte to 2nd,
          ( x << 24 ); // move last byte to start.
}


//returns the float value corresponding to a given bit represention of a scalar int value or vector of int values
float intBitsToFloat(const int32_t x)
{
  union 
  {
    float f;  // assuming 32-bit IEEE 754 single-precision
    int32_t i;    // assuming 32-bit 2's complement int
  } 
  u;

  u.i = x;
  return u.f;
}

double intBitsToDouble(const int64_t x)
{
  union 
  {
    double f;
    int64_t i;
  } 
  u;

  u.i = x;
  return u.f;
}

void CharToTimeStamp(const char* Buffer, double& year, double& month, double& day, double& hour, double& min, double& sec){
  int64_t int64Buffer;

  memcpy(&int64Buffer, &Buffer[0], 8);

  // seconds : double
  sec = intBitsToDouble(int64Buffer);

  memcpy(&int64Buffer, &Buffer[8], 8);

  // minuites : byte
  min = (int64Buffer & 0x00000000000000FF);

  // hours: byte
  hour = ((int64Buffer & 0x000000000000FF00) >> 8);

  // days: byte
  day = ((int64Buffer & 0x0000000000FF0000) >> 16);

  // month : byte
  month = ((int64Buffer & 0x00000000FF000000) >> 24);

  // years : word 
  year = ((int64Buffer & 0x0000FFFF00000000) >> 32);
}

void HeaderRO(ifstream* data, bool printheader, int SegTrig, int max_camp, int* SegCount, bool* active_run, bool* bad, uint *datapos, bool* header_printed, int* camp, int* FirstValidPoint, float* HorizontalInterval, float* VerticalGain, float* VerticalOffset, int* Type, Double_t* day, Double_t* month, Double_t* year, Double_t* hour, Double_t* mnt, Double_t* sec, double* TrigTime, bool corrupted)
{
  int byteoff = 0;
  int Endian = 0;
  int BlockLength = 0; 
  int HeaderLength = 0;
  int TrigTimeLength = 0;
  int WaveArrayCount = 0;
  int PtsPerScreen = 0;
  int LastValidPoint = 0;
  int SegIndex = 0;
  int TimeBase = 0;
  float trigtime = 0;
  int8_t int8Buffer = 0;
  int16_t int16Buffer = 0;
  int32_t int32Buffer = 0;
  int64_t int64Buffer = 0;
  double tempdouble;
  char buffer[16];
  char buffer32[32];
  string pip;
  
  if(!(*header_printed)){
    // find beginning of the header
    (*data).seekg(0, ios::beg);
    (*data).read(buffer32, 32);
    pip = buffer32;
    byteoff = pip.find_first_of("WAVEDESC");
    if(printheader && !(*header_printed)){
      //cout << "\nByte Offset = " << byteoff << endl;
    }
    // get waveform information

    // <32> TYPE : enum 
    (*data).seekg(byteoff+32, ios::beg);
    (*data).read(buffer, 4);
    memcpy(&int16Buffer, buffer, 2);
    *Type = int16Buffer;
    if(*Type==0){
      //cout<< "Type = Byte" << endl;
    }
    if(*Type==1){
      //cout<< "Type = Word" << endl;
    }
    
    // <32> Endian : enum 
    (*data).seekg(byteoff+32, ios::beg);
    (*data).read(buffer, 2);
    memcpy(&int16Buffer, buffer, 2);
    Endian = int16Buffer;
    //cout<< "Endian = " << Endian << endl;
    
    // <byte_position> variable_name : datatype
    //
    // <36> WAVE_DESCRIPTOR : long 
    (*data).seekg(byteoff+36, ios::beg);
    (*data).read(buffer, 4);
    memcpy(&int32Buffer, buffer, 4);
    HeaderLength = int32Buffer;
    if(printheader && !(*header_printed)){
      cout<< "Header Lenght = " << HeaderLength << endl;
    }
    // <40> BLOCK_LENGHT : long 
    (*data).seekg(byteoff+40, ios::beg);
    (*data).read(buffer, 4);
    memcpy(&int32Buffer, buffer, 4);
    BlockLength = int32Buffer;
    if(printheader && !(*header_printed)){
      cout<< "Block Length = " << BlockLength << endl;
    }
    // <48> TRIGTIME_LENGHT : long 
    (*data).seekg(byteoff+48, ios::beg);
    (*data).read(buffer, 4);
    memcpy(&int32Buffer, buffer, 4);
    TrigTimeLength = int32Buffer;
    if(printheader && !(*header_printed)){
      cout<< "TrigTime Length = " << TrigTimeLength << endl;
    }
    // <116> WAVEARRAY_COUNT : long 
    (*data).seekg(byteoff+116, ios::beg);
    (*data).read(buffer, 4);
    memcpy(&int32Buffer, buffer, 4);
    WaveArrayCount = int32Buffer;
    if(printheader && !(*header_printed)){
      cout<< "WaveArrayCount = " << WaveArrayCount << endl;
    }
    // <124> FIRST_VALID_PNT : long 
    (*data).seekg(byteoff+124, ios::beg);
    (*data).read(buffer, 4);
    memcpy(&int32Buffer, buffer, 4);
    *FirstValidPoint = int32Buffer;
    if(printheader && !(*header_printed)){
      cout<< "First Valid Point = " << *FirstValidPoint << endl;
    }
    // <140> SEGINDEX : long 
    (*data).seekg(byteoff+140, ios::beg);
    (*data).read(buffer, 4);
    memcpy(&int32Buffer, buffer, 4);
    SegIndex = int32Buffer;
    if(printheader && !(*header_printed)){
      cout<< "SegIndex = " << SegIndex << endl;
    }
    // <144> SEGCOUNT : long 
    (*data).seekg(byteoff+144, ios::beg);
    (*data).read(buffer, 4);
    memcpy(&int32Buffer, buffer, 4);
    *SegCount = int32Buffer;
    if(printheader && !(*header_printed)){
      cout<< "SegCount = " << *SegCount << endl;
    }
    
    // <326> TIMEBASE : enum not working
    (*data).seekg(byteoff+326, ios::beg);
    (*data).read(buffer, 2);
    memcpy(&int16Buffer, buffer, 2);
    TimeBase = int(int16Buffer);
    if(printheader && !(*header_printed)){
      cout<< "TimeBase = " << int16Buffer << endl;
    }
    // <128> LAST_VALID_PNT : long 
    (*data).seekg(byteoff+128, ios::beg);
    (*data).read(buffer, 4);
    memcpy(&int32Buffer, buffer, 4);
    LastValidPoint = int32Buffer;
    if(printheader && !(*header_printed)){
      cout<< "Last Valid Point = " << LastValidPoint << endl;
    }
    // <156> VERTICAL_GAIN : float
    (*data).seekg(byteoff+156, ios::beg);
    (*data).read(buffer, 4);
    memcpy(&int32Buffer, buffer, 4);
    *VerticalGain = intBitsToFloat(int32Buffer);
    if(printheader && !(*header_printed)){
      cout<< "Vertical Gain = " << *VerticalGain << endl;
    }
    
    // <160> VERTICAL_OFFSET : float
    (*data).seekg(byteoff+160, ios::beg);
    (*data).read(buffer, 4);
    memcpy(&int32Buffer, buffer, 4);
    *VerticalOffset = intBitsToFloat(int32Buffer);
    if(printheader && !(*header_printed)){
      cout<< "Vertical Offset = " << *VerticalOffset << endl;
    }
    // <176> VERTICAL_OFFSET : float
    (*data).seekg(byteoff+176, ios::beg);
    (*data).read(buffer, 4);
    memcpy(&int32Buffer, buffer, 4);
    *HorizontalInterval = intBitsToFloat(int32Buffer);
    if(printheader && !(*header_printed)){
      cout<< "Horizontal Interval = " << *HorizontalInterval << endl;
    }
    // <296> TRIGGER_TIME : time_stamp
    (*data).seekg(byteoff+296, ios::beg);
    (*data).read(buffer, 16);
    CharToTimeStamp(buffer, *year, *month, *day, *hour, *mnt, *sec);
    if(printheader && !(*header_printed)){
      printf("%4.0lf %02.0lf %02.0lf %02.0lf %02.0lf %lf", *year, *month, *day, *hour, *mnt, *sec);
      cout << endl;
    }

    // set Npoints
    bool arraycut=false;
    *camp = (WaveArrayCount)/(*SegCount);
  }    
  if(printheader && !(*header_printed)){
    *header_printed=true;
  }
  // Trigger Timestamp
  (*data).seekg(byteoff+HeaderLength+16*(SegTrig-1), ios::beg);
  (*data).read(buffer, 8);
  memcpy(&int64Buffer, buffer, 8);
  tempdouble = intBitsToDouble(int64Buffer);
  *TrigTime = tempdouble;
  //cout << "TrigTime=" << TrigTime << endl;
  *datapos = byteoff+HeaderLength+TrigTimeLength+((*Type)+1)*((SegTrig)-1)*((WaveArrayCount)/(*SegCount))+(*FirstValidPoint);
  
  return;
}

void EventRO(ifstream* data, uint datapos, int camp, int SegTrig, vector<double_t> &time, vector<double_t> &amp, int Type, int FirstValidPoint, float HorizontalInterval, float VerticalGain, float VerticalOffset){
  (*data).seekg(datapos, ios::beg);
  int8_t int8Buffer = 0;
  int16_t int16Buffer = 0;
  
  uint scaling=10000; //number of samples for a longbuffer. Larger values guarantee less data read operation.
  char longbuffer[2*scaling];
  char buffer[2];
  uint step=0; // sample number within a longbuffer segment

  for(int j=0; j<camp; j++){
    if(Type == 0){
      if(j%scaling==0 && camp-j>scaling){
        (*data).read(longbuffer, 1*scaling);
        step=0;
      }
      else if(j%scaling==0 && camp-j<=scaling){
        (*data).read(longbuffer, camp-j);
        step=0;
      }
      buffer[0]=longbuffer[step];
      step=step+1;
      memcpy(&int8Buffer, buffer, 1);
      time.at(j) = ((j+FirstValidPoint)*HorizontalInterval*1e9);
      amp.at(j) = ((int8Buffer*VerticalGain-VerticalOffset)*1e3);
    }
    else if(Type == 1){
      if(j%scaling==0 && camp-j>scaling){
        (*data).read(longbuffer, 2*scaling);
        step=0;
      }
      else if(j%scaling==0 && camp-j<=scaling){
        (*data).read(longbuffer, 2*(camp-j));
        step=0;
      }
      buffer[0]=longbuffer[step];
      buffer[1]=longbuffer[step+1];
      step=step+2;
      memcpy(&int16Buffer, buffer, 2);
      time.at(j) = ((j+FirstValidPoint)*HorizontalInterval*1e9);
      amp.at(j) = ((int16Buffer*VerticalGain-VerticalOffset)*1e3);
    }
  }

  return;
}

void EventROdump(char* data, uint datapos, int camp, int campfact, vector<double_t> &time, vector<double_t> &amp, vector<double_t> &m_amp, int polarity, int Type, int FirstValidPoint, float HorizontalInterval, float VerticalGain, float VerticalOffset){
  int8_t int8Buffer = 0;
  int16_t int16Buffer = 0;
  
  uint scaling=10000; //number of samples for a longbuffer. Larger values guarantee less data read operation.
  char longbuffer[2*scaling];
  char buffer[2];
  uint step=0; // sample number within a longbuffer segment
  cout << "length " << data[1] << endl;
  for(int j=0; j<camp+1; j++){
    if(Type == 0){
      if(j%scaling==0 && camp-j>scaling){
        for(int bit=0;bit<scaling;bit++){
          longbuffer[bit]=data[datapos+bit];
        }
        datapos=datapos+scaling;
        step=0;
      }
      else if(j%scaling==0 && camp-j<=scaling){
        for(int bit=0;bit<camp-j;bit++){
          longbuffer[bit]=data[datapos+bit];
        }
        datapos=datapos+camp-j;
        step=0;
      }
      buffer[0]=longbuffer[step];
      step=step+1;
      memcpy(&int8Buffer, buffer, 1);
      time.at(j) = ((j+FirstValidPoint)*HorizontalInterval*1e9);
      amp.at(j) = (polarity*(int8Buffer*VerticalGain-VerticalOffset)*1e3);
    }
    else if(Type == 1){
      if(j%scaling==0 && camp-j>scaling){
        for(int bit=0;bit<2*scaling;bit++){
          longbuffer[bit]=data[datapos+bit];
        }
        datapos=datapos+2*scaling;
        step=0;
      }
      else if(j%scaling==0 && camp-j<=scaling){
        for(int bit=0;bit<2*(camp-j);bit++){
          longbuffer[bit]=data[datapos+bit];
        }
        datapos=datapos+2*(camp-j);
        step=0;
      }
      buffer[0]=longbuffer[step];
      buffer[1]=longbuffer[step+1];
      step=step+2;
      memcpy(&int16Buffer, buffer, 2);
      time.at(j) = ((j+FirstValidPoint)*HorizontalInterval*1e9);
      amp.at(j) = (polarity*(int16Buffer*VerticalGain-VerticalOffset)*1e3);
    }
    m_amp.at(j) = (amp.at(j));

  }

  return;
}


#endif
