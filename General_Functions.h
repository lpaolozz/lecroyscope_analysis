#ifndef GENERAL_FUNCTIONS_H
#define GENERAL_FUNCTIONS_H


using namespace std;

string replaceChar(string str, char ch1, char ch2) {
  for (int i = 0; i < str.length(); ++i) {
    if (str[i] == ch1)
      str[i] = ch2;
  }

  return str;
}

string ReplaceFirstOccurrence(std::string& s, const std::string& toReplace, const std::string& replaceWith){
    std::size_t pos = s.find(toReplace);
    if (pos == std::string::npos) 
      return s;
    return s.replace(pos, toReplace.length(), replaceWith);
}

string ReplaceString(std::string subject, const std::string& search, const std::string& replace) {
  size_t pos = 0;
  while ((pos = subject.find(search, pos)) != std::string::npos) {
    subject.replace(pos, search.length(), replace);
    pos += replace.length();
  }
  return subject;
}

string EraseAllSubStr(std::string& mainStr, const std::string & toErase)
{
  size_t pos = std::string::npos;
  // Search for the substring in string in a loop until nothing is found
  while ((pos  = mainStr.find(toErase) )!= std::string::npos)
  {
      // If found then erase it from string
      mainStr.erase(pos, toErase.length());
  }
  return mainStr;
}

// Removes spaces from a string
void removeSpaces(char *str)
{
    // To keep track of non-space character count
    int count = 0;
 
    // Traverse the given string. If current character
    // is not space, then place it at index 'count++'
    for (int i = 0; str[i]; i++)
        if (str[i] != ' ')
            str[count++] = str[i]; // here count is
                                   // incremented
    str[count] = '\0';
}

double deval(string str, double amp[], double toa[], double tot[], double noise[]){
  size_t par=str.find('[');
  if(par<str.length()){
    int i=stoi(str.substr(par+1,1));
    if(str=="amp["+ to_string(i) + "]"){
      return amp[i];
    }
    if(str=="toa["+ to_string(i) + "]"){
      return toa[i];
    }
    if(str=="tot["+ to_string(i) + "]"){
      return tot[i];
    }
    if(str=="noise["+ to_string(i) + "]"){
      return noise[i];
    }
  }
  
  return stod(str);
}

bool is_number(const std::string& s){
  std::string::const_iterator it = s.begin();
  while(it != s.end() && std::isdigit(*it)){
    ++it;
  }
  return !s.empty() && it == s.end();
}

// Command line arguments

// Checks if there are command line options
bool cmdOptionExists(char** begin, char** end, const std::string& option){
  return std::find(begin, end, option) != end;
}

// Gets command line option
char* get1stCmdOption(char ** begin, char ** end, const std::string & option){
  char ** itr = std::find(begin, end, option);
  if (itr != end && ++itr != end){
    return *itr;
  }
  return 0;
}

char* get2ndCmdOption(char ** begin, char ** end, const std::string & option){
  char ** itr = std::find(begin, end, option);
  if(itr != end && ++itr != end && ++itr != end){
    return *itr;
  }
  return 0;
}


#endif
