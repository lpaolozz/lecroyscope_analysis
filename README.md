# LecroyScope_Analysis

Developer: lorenzo.paolozzi@cern.ch
Reporters: stefano.zambito@cern.ch, matteo.milanesio@cern.ch, theo.moretti@cern.ch

Software for data analysis in C++ and ROOT for binary data with Lecroy scopes

Compatible with Linux.
Partly compatible with mac (edit on the namespace for filesystem may be required).
Require recent gcc compiler (c++17) to use filesystem library.

WAVEFORM CONVERSION

Analysis_Lecroy.cpp should be compiled with the command:
g++ `root-config --cflags` -o Analysis_Lecroy Analysis_Lecroy.cpp `root-config --glibs`


Usage:

./Analysis_Lecroy RunList.csv

or

./Analysis_Lecroy [full_path_to_input_folder]

RunList should be structured as in the RunList_template.csv

Input files should be binary files (.trc), acquired either in byte (8-bit) or word (16-bit) format.

NOTE: I suggest to create the CSV file with an regular text editor (no excel), unless verified that it works properly.

The InputCARD (generated in the root folder after the first use of the program) sets the parameters for the analysis, such as the region for background and signal analysis, or expected pulse polarity. The default units for time and amplitude are ns and mV, respectively.

Since version 2.0 multiple output channels can be defined with different analyses on them. The naming of variable in the output tree reflects the output channel index and not the corresponding input. The association of input to output is now done in the InputCARD.

NOTE: Acquired channel index does not correspond to the scope channel. If one channel is acquired, it will be input channel 1 in the InputCARD. If N channels are acquired, they will be input channels from 1 to N in the InputCARDthe root file, starting from the one with the lowest scope index and increasing accordingly. This feature may be changed in future upgrades to to reflect the actual scope channel index, so stay posted.

WAVEFORM VIEWER

Wave_Plotter.cpp should be compiled with the command:
g++ `root-config --cflags` -o Wave_Plotter Wave_Plotter.cpp `root-config --glibs`


Usage:

./Wave_Plotter full_path_to_input_folder