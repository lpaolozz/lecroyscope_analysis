#ifndef SETUP_H
#define SETUP_H

#include <iostream>
#include <string>
#include <iomanip>

#ifndef __has_include
  static_assert(false, "__has_include not supported");
  #else
  #  if __cplusplus >= 201703L && __has_include(<filesystem>)
  #    include <filesystem>
       namespace fs = std::filesystem;
  #  elif __cplusplus < 201703L &&__has_include(<filesystem>)
  #    include <filesystem>
       namespace fs = std::__fs::filesystem;
  #  elif __has_include(<experimental/filesystem>)
  #    include <experimental/filesystem>
       namespace fs = std::experimental::filesystem;
  #  elif __has_include(<boost/filesystem.hpp>)
  #    include <boost/filesystem.hpp>
       namespace fs = boost::filesystem;
  #  endif
#endif

using namespace std;

void CardSETUP(std::ifstream* InputCARD, int* nchro, int* nchout, string outdef[], bool doFFT[], int showFFT[], bool domobileavg[], string lowpass[], Double_t fcuthigh[], Double_t fcutlow[], int showwave[], bool* printheader, bool stdvariables[], bool doshift[], Double_t shift[], int campfact[], Double_t t_bck_low[], Double_t t_bck_high[], Double_t t_pul_low[], Double_t t_pul_high[], int nthr[], Double_t thr[][100], int polarity[], bool askforcard, bool debug[])
{
  std::string selection="";
  std::stringstream ss;
  ss.str("");
  ss.clear();
  bool newcard=false;
  bool generating_output = true;
  *nchout = 0;
  string valid_commands[7]={"do_mobile_avg","do_shift","std_variables","do_FFT","show_waveform","thresholds","show_FFT"};
  string help_string = "List of possible commands: \ndo_mobile_avg \ndo_shift \nstd_variables \ndo_FFT \nshow_waveform \nshow_FFT\n";

  if(*InputCARD && askforcard){
    selection="";
    std::cin.clear();
    std::cout << "InputCARD found. Do you want to create a new InputCARD? (y/N)" << std::endl;
    getline(std::cin, selection, '\n' );
    if(selection=="y" || selection=="yes" || selection=="Y"){
    newcard=true;
    }
  }
  
  // If there is no inputCARD, or if the user wants to create a new one: creating a new input card.
  if(!(*InputCARD) || newcard){
    if(!(*InputCARD)){
      std::cout << "Error: could not find the InputCARD, default settings will be used and a new InputCARD will be created" << std::endl<< std::flush;
    }
    else
    {
      (*InputCARD).close();
      remove("InputCARD.in");
    }
    std::ofstream InputSTD("InputCARD.in", std::ofstream::out);


    selection="";
    std::cin.clear();
    std::cout << std::endl << "Do you want to print the headers during conversion? (y/N)" << std::endl;
    getline(std::cin, selection, '\n' );
    if(selection=="y" || selection=="yes" || selection=="Y"){
      *printheader=true;
    }
    InputSTD << "print_headers\t{" <<   *printheader << "}\n";

    selection="";
    std::cin.clear();
    std::cout << std::endl << "Set number of input channels (up to 16 channels allowed)" << std::endl;
    getline(std::cin, selection, '\n' );
    *nchro = stoi(selection);
    InputSTD << "number_of_input_channels\t{" <<   *nchro << "}\n";

    // In this cycle the user writes down the commands for the output channels
    while(generating_output){
      bool newfunction = true;
      bool pulseanalysis = false;
      InputSTD << "\nout_channel\n";

      // Defining input of analysis channel
      selection="";
      std::cin.clear();
      std::cout << std::endl << "Define input for output channel " << (*nchout)+1 << ". " << std::endl << "Examples: 1; 2; 1-2; 2-1; 3+4; etc..." << std::endl;
      getline(std::cin, selection, '\n' );
      selection.erase(remove_if(selection.begin(), selection.end(), ::isspace), selection.end()); // removing spaces because users do unnecessary stuff...
      outdef[*nchout] = selection;
      InputSTD << "\nuse_input\t{" << outdef[*nchout] << "}\n";

      // Setting the window for background analysis
      selection="";
      std::cin.clear();
      std::cout << std::endl << "Set the lower time limit [ns] at which you expect to have only background for analysis channel " << (*nchout)+1 << std::endl;
      getline(std::cin, selection, '\n' );
      t_bck_low[*nchro] = stof(selection);
      std::cout << std::endl << "Set the upper time limit [ns] at which you expect to have only background for analysis channel " << (*nchout)+1 << std::endl;
      selection="";
      getline(std::cin, selection, '\n' );
      t_bck_high[*nchro] = stof(selection);
      if(fabs(t_bck_high[*nchro])<0.0001){
        std::cout << "WARNING: Upper limit for background cannot be zero! It will be set to 50 ns." << endl;
        t_bck_high[*nchro] = 10;
      }
      if(t_bck_low[*nchro]>t_bck_high[*nchro]){
        std::cout << "WARNING: Lower limit for background cannot be larger than upper limit! It will be set to 10 ns below the upper limit." << endl;
        t_bck_low[*nchro] = t_bck_high[*nchro]-10;
      }
      InputSTD << "background_region\t{" << t_bck_low[*nchro] << "," << t_bck_high[*nchro] << "}\n";

      // Setting  
      while(newfunction){
        selection="";
        std::cin.clear();
        std::cout << std::endl << "Select an analysis command for this channel (type help for help, type done complete selection) " << std::endl;
        getline(std::cin, selection, '\n' );
        if(selection=="help"){
          cout << help_string << endl;
        }
        else if(selection=="done"){
          newfunction = false;
        }
        else if(selection == "std_variables"){
          pulseanalysis = true;
          stdvariables[*nchro] = true;

        }
        else if(selection == "do_mobile_avg"){
          domobileavg[*nchro] = true;
          selection="";
          std::cin.clear();
          std::cout << std::endl << "Type 1 for exponential filter (preferred), 2 for linear filter" << std::endl;
          getline(std::cin, selection, '\n' );
          if(stoi(selection) == 1){
            lowpass[*nchro] = "exp";
          }
          else{
            lowpass[*nchro] = "lin";
          }
          selection="";
          std::cin.clear();
          std::cout << std::endl << "Set cutoff frequency for low-pass filter (in [GHz])" << std::endl;
          getline(std::cin, selection, '\n' );
          fcuthigh[*nchro] = stof(selection);
          InputSTD << "do_mobile_avg\t{" << selection << "," << fcuthigh[*nchro] << "}\n";
          fcuthigh[*nchro] = fcuthigh[*nchro]*1e9; // use in Hz in the program
        }
        else if(selection == "do_shift"){
          doshift[*nchro] = true;
          selection="";
          std::cin.clear();
          std::cout << std::endl << "Set shift (in [ns])" << std::endl;
          getline(std::cin, selection, '\n' );
          shift[*nchro]=stof(selection);
          InputSTD << "do_shift\t{" << shift[*nchro] << "}\n";
        }
        else if(selection == "do_FFT"){
          InputSTD << "do_FFT\n";
          doFFT[*nchro] = true;
        }
        else if(selection == "show_waveform"){
          selection="";
          std::cin.clear();
          std::cout << std::endl << "Select the number of waveforms to record" << std::endl;
          getline(std::cin, selection, '\n' );
          showwave[*nchro]=stoi(selection);
          InputSTD << "show_waveform\t{" << showwave[*nchro] << "}\n";
        }
        else if(selection == "show_FFT"){
          selection="";
          std::cin.clear();
          std::cout << std::endl << "Select the number of FFTs to record" << std::endl;
          getline(std::cin, selection, '\n' );
          showFFT[*nchro] = stoi(selection);
          InputSTD << "show_FFT\t{" << showFFT[*nchro] << "}\n";
        }
        else{
          std::cout << std::endl << "Invalid selection (type help for help, type done to complete selection)" << std::endl;
        }

      }
      if(!pulseanalysis){
        selection="";
        std::cin.clear();
        std::cout << std::endl << "Do you want to do a pulse analysis? (Y/n)" << std::endl;
        getline(std::cin, selection, '\n' );
        if(selection=="y" || selection=="yes" || selection=="Y"){
          pulseanalysis=true;
        }
      }
      if(pulseanalysis){
        // Setting the window for the pulse analysis
        selection="";
        std::cin.clear();
        std::cout << std::endl << "Set the lower time limit [ns] at which you expect to see a pulse for analysis channel " << (*nchout)+1 << std::endl;
        getline(std::cin, selection, '\n' );
        t_pul_low[*nchro] = stof(selection);
        selection = "";
        std::cin.clear();
        std::cout << std::endl << "Set the upper time limit [ns] at which you expect to see a pulse for analysis channel " << (*nchout)+1 << std::endl;
        getline(std::cin, selection, '\n' );
        t_pul_high[*nchro] = stof(selection);
        if(fabs(t_pul_high[*nchro])<0.0001){
          std::cout << "WARNING: Upper limit for pulse cannot be zero! It will be set to 500 ns." << endl;
          t_pul_high[*nchro] = 50;
        }
        if(t_pul_low[*nchro]>t_pul_high[*nchro]){
          std::cout << "WARNING: Lower limit for pulse cannot be larger than upper limit! It will be set to 50 ns below the upper limit." << endl;
          t_pul_low[*nchro] = t_pul_high[*nchro]-50;
        }
        InputSTD << "pulse_region\t{" << t_pul_low[*nchro] << "," << t_pul_high[*nchro] << "}\n";

        // Setting pulse polarity
        selection="";
        std::cin.clear();
        std::cout << std::endl << "Set pulse polarity (1 or -1) for analysis channel " << (*nchout)+1 << std::endl;
        getline(std::cin, selection, '\n' );
        polarity[*nchro] = stoi(selection);
        InputSTD << "polarity\t{" << polarity[*nchout] << "}\n";

        // Setting thresholds
        selection="";
        std::cin.clear();
        std::cout << std::endl << "Set threshold for the analysis, separated by commas. Thresholds should be integer values, in [mV]." << std::endl;
        getline(std::cin, selection, '\n' );
        selection.erase(remove_if(selection.begin(), selection.end(), ::isspace), selection.end());
        InputSTD << "thresholds\t{" + selection + "}\n";
        ss.str("");
        ss.clear();
        ss << selection;
        while(ss.good()){
          string substr;
          getline(ss,substr,',');
          thr[*nchro][nthr[*nchro]] = stoi(substr);
          nthr[*nchro]++;
        }

      }

      // At this point, ask if you want to add output analysis channels
      selection="";
      std::cin.clear();
      std::cout << std::endl << "Do you want to add another input channel? (y/N)" << std::endl;
      getline(std::cin, selection, '\n' );
      if(selection!="y" && selection!="yes" && selection!="Y"){
        generating_output = false;
      }
      (*nchout)++;
    }

    InputSTD.close();
    // The new input card is complete, write here a summary of the input card
    ifstream ff("InputCARD.in",ios::in);
    cout << endl << "Reading back inputCARD:" << endl;
    if (ff.is_open()){
      std::cout << ff.rdbuf();
    }
  }

  std::string pip;
  std::string outfilename;

  // Reading the input card

  string tempstr;
  string substr;
  if((*InputCARD) && !newcard){
    tempstr = "";
    while(tempstr==""){
      getline(*InputCARD, tempstr, '\n' );
    }
    ss.str("");
    ss.clear();
    ss << tempstr;
    getline(ss,substr,'{');
    getline(ss,substr,'}');
    if(stoi(substr) != 0){
      *printheader = true;
    }
    else{
      //cout << "Not printing waveform headers" << endl;
    }

    tempstr = "";
    while(tempstr==""){
      getline(*InputCARD, tempstr, '\n' );
    }
    ss.str("");
    ss.clear();
    ss << tempstr;
    getline(ss,substr,'{');
    getline(ss,substr,'}');
    *nchro = stoi(substr);
    cout << "Reading " << *nchro << " channels" << endl;
    (*nchout) = -1; // start at minus one to increase at the beginning of the iterations
    while(!(*InputCARD).eof()){
      bool newchannel = false;
      tempstr = "";
      while(tempstr=="" && !(*InputCARD).eof()){
        getline(*InputCARD, tempstr, '\n' );
      }
      if(tempstr == "out_channel"){ // a new channel
        (*nchout)++;
      }
      if(tempstr == "debug"){ // a new channel
        cout << "debugging channel " << *nchout+1 << endl;
        debug[*nchout] = true;
      }
      
      ss.str("");
      ss.clear();
      ss << tempstr;
      getline(ss,substr,'{'); // this will contain the name of the command, possibly with spaces or tabs
      substr.erase(remove_if(substr.begin(), substr.end(), ::isspace), substr.end());
      substr.erase(remove(substr.begin(), substr.end(), '\t'), substr.end());
      if(substr == "use_input"){
        getline(ss,substr,'}');
        outdef[*nchout] = substr;
      }
      if(substr == "do_mobile_avg"){
        domobileavg[*nchout] = true;
        getline(ss,substr,',');
        lowpass[*nchout] = substr;
        getline(ss,substr,'}');
        fcuthigh[*nchout] = stof(substr);
        fcuthigh[*nchout] = fcuthigh[*nchout]*1e9;
      }
      if(substr == "do_shift"){
        doshift[*nchout] = true;
        getline(ss,substr,'}');
        shift[*nchout] = stof(substr);
      }
      if(substr == "std_variables"){
        stdvariables[*nchout] = true;
      }
      if(substr == "polarity"){
        getline(ss,substr,'}');
        polarity[*nchout] = stoi(substr);
      }
      if(substr == "do_FFT"){
        doFFT[*nchout] = true;
      }
      if(substr == "show_waveform"){
        getline(ss,substr,'}');
        showwave[*nchout] = stoi(substr);
      }
      if(substr == "show_FFT"){
        getline(ss,substr,'}');
        showFFT[*nchout] = stoi(substr);
      }
      if(substr == "background_region"){
        getline(ss,substr,',');
        t_bck_low[*nchout] = stoi(substr);
        getline(ss,substr,'}');
        t_bck_high[*nchout] = stoi(substr);
      }
      if(substr == "pulse_region"){
        getline(ss,substr,',');
        t_pul_low[*nchout] = stoi(substr);
        getline(ss,substr,'}');
        t_pul_high[*nchout] = stoi(substr);
      }
      if(substr == "thresholds"){
        while(ss.good()){
          getline(ss,substr,',');
          substr.erase(remove(substr.begin(), substr.end(), '}'), substr.end());
          thr[*nchout][nthr[*nchout]] = stoi(substr);
          nthr[*nchout]++;
        }
      }
    
    }
    (*nchout)++; // increase by one at the end, to change the use from index to counter
    if ((*InputCARD).is_open()){
      (*InputCARD).clear();
      (*InputCARD).seekg(0);
      std::cout << (*InputCARD).rdbuf();
    }
  }
  return;
}

void ReadList(std::ifstream* runlist, std::string inpath[], std::string outpath[], std::string firstseg[], std::string lastseg[], int* nruns)
{
  bool running=true;
  std::string pip;
  std::getline( *runlist, pip, ',' );
  std::getline( *runlist, pip, ',' );
  std::getline( *runlist, pip, ',' );
  std::getline( *runlist, pip, '\n' );
  while(running){
    if((*runlist).eof()){
      running=false;
    }
    if(running){
      std::getline(*runlist, inpath[*nruns], ',' );
      std::getline(*runlist, outpath[*nruns], ',' );
      std::getline(*runlist, firstseg[*nruns], ',' );
      std::getline(*runlist, lastseg[*nruns], '\n' );
      if(!fs::exists(inpath[*nruns])){
        if(inpath[*nruns] == ""){
          cout << endl << "WARNING! Empty line in RunList, skipping line: " << *nruns+1 << "." << endl;
        }
        else{
          cout << endl << "Path not found: " << inpath[*nruns] << endl;
          cout << "WARNING! Wrong path in RunList, skipping line: " << *nruns+1 << "." << endl;
        }
        inpath[*nruns]="";
        outpath[*nruns]="";
        firstseg[*nruns]="";
        lastseg[*nruns]="";
      }
      else{
        (*nruns)++;
      }
    }
  }
}

int RunMatrix( int n, uint nfirstseg, uint nlastseg, int runindex[], vector<std::string> *runmatrix, std::string inpath[], std::string outpath[])
{
  int fillindex;
  std::string tempstring;
  std::string format;
  int filled[8];
  bool search=true;
  std::string path = inpath[n].c_str();
  std::vector<std::string> vpath; // vector containing the paths to the files in the input folder
  for (const auto & entry : fs::directory_iterator(path)){
    //std::cout << entry.path() << std::endl;
    vpath.push_back(entry.path());
  }
  sort(vpath.begin(), vpath.end()); 


  // Placing the file names on a matrix
  for(int i=0;i<8;i++){
    runindex[i]=0;
    filled[i]=0;
  }
  fillindex=0;
  for(int j=0;j<vpath.size();j++)
  {
    search=true;
    tempstring="";
    tempstring=vpath.at(j);
    //std::cout << "tempstring=" << tempstring << std::endl;
    std::size_t pos = tempstring.find_last_of(".");
    std::string ext = tempstring.substr(pos+1);
    format = ext;
    if(ext=="trc"){
      //std::cout << "it's a binary" << std::endl;
    }
    else if(ext=="csv"){
      std::cerr << "it's a csv file! Decode not possible" << std::endl;
      return(0);
    }
    else{
      std::cerr << "unknown format " << ext << std::endl;
    return(0);
    }
    int k=0;
    while(k<tempstring.length() && search){
      k++;
      if(tempstring.substr(pos-k,1) == "/"){
        search=false;
        //std::cout << "k=" << k << std::endl;
      }
    }
    if(search){
      std::cerr << "Bad path!";
      return(0);
    }
    if(stol(tempstring.substr(tempstring.length()-9,5))>=nfirstseg && stol(tempstring.substr(tempstring.length()-9,5))<=nlastseg){
      if(tempstring.substr(pos-(k-1),2)=="C1"){
        runmatrix[0].push_back(tempstring);
        //runmatrix[0][runindex[0]]=tempstring;
        runindex[0]++;
        filled[0]=1;
      }
      if(tempstring.substr(pos-(k-1),2)=="C2"){
        fillindex=filled[0];
        runmatrix[fillindex].push_back(tempstring);
        //runmatrix[fillindex][runindex[fillindex]]=tempstring;
        runindex[fillindex]++;
        filled[1]=1;
      }
      if(tempstring.substr(pos-(k-1),2)=="C3"){
        fillindex=filled[0]+filled[1];
        runmatrix[fillindex].push_back(tempstring);
        //runmatrix[fillindex][runindex[fillindex]]=tempstring;
        runindex[fillindex]++;
        filled[2]=1;
      }
      if(tempstring.substr(pos-(k-1),2)=="C4"){
        fillindex=filled[0]+filled[1]+filled[2];
        runmatrix[fillindex].push_back(tempstring);
        //runmatrix[fillindex][runindex[fillindex]]=tempstring;
        runindex[fillindex]++;
      }
    }
  }
  return(1);
}

void GeneralBranchDefinition(TTree* EventData, Int_t* ntrig, Int_t* SegTrig, Int_t* nseg, Int_t* nrun, Int_t* nchro, Double_t* year, Double_t* month, Double_t* day, Double_t* hour, Double_t* mnt, Double_t* sec, double* TrigTime){
  *EventData->Branch("ntrig",ntrig,"ntrig/I");
  *EventData->Branch("SegTrig",SegTrig,"SegTrig/I");
  *EventData->Branch("nseg",nseg,"nseg/I");
  *EventData->Branch("nrun",nrun,"nrun/I");
  *EventData->Branch("nchro",nchro,"nchro/I");
  *EventData->Branch("year",year,"year/D");
  *EventData->Branch("month",month,"month/D");
  *EventData->Branch("day",day,"day/D");
  *EventData->Branch("hour",hour,"hour/D");
  *EventData->Branch("mnt",mnt,"mnt/D");
  *EventData->Branch("sec",sec,"sec/D");
  *EventData->Branch("TrigTime",TrigTime,"TrigTime/d");
  return;
}

void BaseBranchDefinition(TTree* EventData, int i, Double_t* t_bck_low,  Double_t* t_bck_high, Double_t* bck, Double_t* max_bck, Double_t* rms_bck, Double_t* max_time, Double_t* max_time_bck, Double_t* max, Double_t* pulsecharge){
  ostringstream leaf(ostringstream::out);
  ostringstream leafl(ostringstream::out);

  leaf.str("");leaf.clear();leafl.str("");leafl.clear();
  leaf << "t_bck_low" << i+1;
  leafl << "t_bck_low" << i+1 <<"/D";
  *EventData->Branch(leaf.str().c_str(),t_bck_low,leafl.str().c_str());

  leaf.str("");leaf.clear();leafl.str("");leafl.clear();
  leaf << "t_bck_high" << i+1;
  leafl << "t_bck_high" << i+1 <<"/D";
  *EventData->Branch(leaf.str().c_str(),t_bck_high,leafl.str().c_str());

  leaf.str("");leaf.clear();leafl.str("");leafl.clear();
  leaf << "bck" << i+1;
  leafl << "bck" << i+1 <<"/D";
  *EventData->Branch(leaf.str().c_str(),bck,leafl.str().c_str());

  leaf.str("");leaf.clear();leafl.str("");leafl.clear();
  leaf << "max_bck" << i+1;
  leafl << "max_bck" << i+1 <<"/D";
  *EventData->Branch(leaf.str().c_str(),max_bck,leafl.str().c_str());

  leaf.str("");leaf.clear();leafl.str("");leafl.clear();
  leaf << "rms_bck" << i+1;
  leafl << "rms_bck" << i+1 <<"/D";
  *EventData->Branch(leaf.str().c_str(),rms_bck,leafl.str().c_str());

  leaf.str("");leaf.clear();leafl.str("");leafl.clear();
  leaf << "max_time" << i+1;
  leafl << "max_time" << i+1 << "/D";
  *EventData->Branch(leaf.str().c_str(), max_time, leafl.str().c_str());

  leaf.str("");leaf.clear();leafl.str("");leafl.clear();
  leaf << "max_time_bck" << i+1;
  leafl << "max_time_bck" << i+1 << "/D";
  *EventData->Branch(leaf.str().c_str(), max_time_bck, leafl.str().c_str());

  leaf.str("");leaf.clear();leafl.str("");leafl.clear();
  leaf << "max" << i+1;
  leafl << "max" << i+1 << "/D";
  *EventData->Branch(leaf.str().c_str(), max, leafl.str().c_str());

  leaf.str("");leaf.clear();leafl.str("");leafl.clear();
  leaf << "charge" << i+1;
  leafl << "charge" << i+1 << "/D";
  *EventData->Branch(leaf.str().c_str(), pulsecharge, leafl.str().c_str());
      
  return;
}
void ThrBranchDefinition(TTree* EventData, int i, Double_t thr, Double_t* time_thr, Double_t* trail_thr){
  
  ostringstream leaf(ostringstream::out);
  ostringstream leafl(ostringstream::out);
  leaf.str("");leaf.clear();leafl.str("");leafl.clear();
  leaf << "time_"<< thr << "thr" << i+1;
  leafl << "time_" << thr << "thr" << i+1 << "/D";
  *EventData->Branch(leaf.str().c_str(), time_thr, leafl.str().c_str());

  leaf.str("");leaf.clear();leafl.str("");leafl.clear();
  leaf << "trail_"<< thr << "thr" << i+1;
  leafl << "trail_" << thr << "thr" << i+1 << "/D";
  *EventData->Branch(leaf.str().c_str(), trail_thr, leafl.str().c_str());

}



void StandardBranchDefinition(TTree* EventData,int i, Double_t* time_10const, Double_t* time_20const, Double_t* time_50const, Double_t* time_80const, Double_t* time_90const, Double_t* time_3rms, Double_t* time_5rms){
  ostringstream leaf(ostringstream::out);
  ostringstream leafl(ostringstream::out);
  leaf.str("");leaf.clear();leafl.str("");leafl.clear();
  leaf << "time_10const" << i+1;
  leafl << "time_10const" << i+1 << "/D";
  *EventData->Branch(leaf.str().c_str(), time_10const, leafl.str().c_str());

  leaf.str("");leaf.clear();leafl.str("");leafl.clear();
  leaf << "time_20const" << i+1;
  leafl << "time_20const" << i+1 << "/D";
  *EventData->Branch(leaf.str().c_str(), time_20const, leafl.str().c_str());

  leaf.str("");leaf.clear();leafl.str("");leafl.clear();
  leaf << "time_50const" << i+1;
  leafl << "time_50const" << i+1 << "/D";
  *EventData->Branch(leaf.str().c_str(), time_50const, leafl.str().c_str());

  leaf.str("");leaf.clear();leafl.str("");leafl.clear();
  leaf << "time_80const" << i+1;
  leafl << "time_80const" << i+1 << "/D";
  *EventData->Branch(leaf.str().c_str(), time_80const, leafl.str().c_str());

  leaf.str("");leaf.clear();leafl.str("");leafl.clear();
  leaf << "time_90const" << i+1;
  leafl << "time_90const" << i+1 << "/D";
  *EventData->Branch(leaf.str().c_str(), time_90const, leafl.str().c_str());

  leaf.str("");leaf.clear();leafl.str("");leafl.clear();
  leaf << "time_3rms" << i+1;
  leafl << "time_3rms" << i+1 << "/D";
  *EventData->Branch(leaf.str().c_str(), time_3rms, leafl.str().c_str());

  leaf.str("");leaf.clear();leafl.str("");leafl.clear();
  leaf << "time_5rms" << i+1;
  leafl << "time_5rms" << i+1 << "/D";
  *EventData->Branch(leaf.str().c_str(), time_5rms, leafl.str().c_str());
  
  return;
}

void RecordBranchDefinition(TTree* EventData,int i, vector<double_t> *timerec, vector<double_t> *amprec){
  ostringstream leaf(ostringstream::out);
  ostringstream leafl(ostringstream::out);

  leaf.str("");leaf.clear();leafl.str("");leafl.clear();
  leaf << "wave_time" << i+1;
  *EventData->Branch(leaf.str().c_str(), timerec);
      
  leaf.str("");leaf.clear();leafl.str("");leafl.clear();
  leaf << "wave_amp" << i+1;
  *EventData->Branch(leaf.str().c_str(), amprec);
  return;
}

void FFTBranchDefinition(TTree* EventData,int i, vector<double_t> *freq, vector<double_t> *FFT_real, vector<double_t> *FFT_imag, vector<double_t> *FFT_abs){
  ostringstream leaf(ostringstream::out);
  ostringstream leafl(ostringstream::out);

  leaf.str("");leaf.clear();leafl.str("");leafl.clear();
  leaf << "freq" << i+1;
  *EventData->Branch(leaf.str().c_str(), freq);

  leaf.str("");leaf.clear();leafl.str("");leafl.clear();
  leaf << "FFT_real" << i+1;
  *EventData->Branch(leaf.str().c_str(), FFT_real);

  leaf.str("");leaf.clear();leafl.str("");leafl.clear();
  leaf << "FFT_imag" << i+1;
  *EventData->Branch(leaf.str().c_str(), FFT_imag);

  leaf.str("");leaf.clear();leafl.str("");leafl.clear();
  leaf << "FFT_abs" << i+1;
  *EventData->Branch(leaf.str().c_str(), FFT_abs);
  return;
}
#endif
