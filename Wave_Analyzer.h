#ifndef WAVE_ANALYZER_H
#define WAVE_ANALYZER_H

#include <iostream>

//Always run background function before doing further analyses

//Compute average amplitude for background
//(record lenght, amplitude vector, time vector, upper limit for background, upper limit for pulse, average background amplitude, max background amplitude)
void Background(vector<double_t> time, vector<double_t> amp, Double_t t_bck_low, Double_t t_bck_high, Double_t *bck, Double_t *maxbck, Double_t *rmsbck, Double_t *max_time_bck)
{
  int camp = time.size();
  double min=1000000;
  double max=-1000000;
  float sumamp=0;
  float sumampsq=0;
  int pointsavg=0;
  float meanbck;
  *max_time_bck = -1;
  *bck=0;
  for(int j=0;j<camp;j++){
    if((time.at(j)>t_bck_low && time.at(j)<t_bck_high))
    {
      if(amp.at(j)>max){
        max=amp.at(j);
        *max_time_bck = time.at(j);
      }
      if(amp.at(j)<min){
        min=amp.at(j);
      }
      pointsavg++;
      sumamp=sumamp+amp.at(j);
      sumampsq=sumampsq+amp.at(j)*amp.at(j);
    }
  }
  meanbck=sumamp/float(pointsavg);
  *rmsbck=sqrt((1./float(pointsavg))*(sumampsq-float(pointsavg)*meanbck*meanbck));
  *bck=meanbck;
  *maxbck=max-*bck;
  return;
}

// Compute maximum, amplitude and peak time for waveform
//(record lenght, amplitude vector, time vector, upper limit for background, upper limit for pulse, average background amplitude, pulse maximum amplitude (averaged), pulse maximum amplitude)
void Amplitudes(vector<double_t> time, vector<double_t> amp,  Double_t bck, Double_t rms_bck, Double_t t_pul_low,Double_t t_pul_high, Double_t *max,Double_t *tmax, Double_t *pamp, Double_t *tamp, Double_t *chi)
{
  int camp = time.size();
  int np=0;
  int cont=0;
  int j_max;
  int j_start;
  int n_j = 0;
  double tempmean;
  double tempsig;
  *max=-100000;
  

  //Maximum calculation
  for(int j=0;j<camp;j++){

    if(time.at(j)>t_pul_low && time.at(j)<t_pul_high){
      np++;
      if(amp.at(j)-bck>*max){
        *max=amp.at(j)-bck;
        *tmax=(time.at(j+1)+time.at(j))/2.;
        j_max=j;
      }
    }
  }
  
  //Fit amplitude calculation
  if((*max)<3*rms_bck){
    *pamp = -9999;
    return;
  }

  // This routine fails on MAC, temporarily commented

  /*
  bool search=true;
  for(int j=j_max;j>1 && search;j--)
  {
    if(amp.at(j)-bck<(*max)-3*rms_bck || j_max-j>100)
    {
      search=false;
      j_start=j;
    }
  }
  search=true;
  for(int j=j_start+1;j<camp-1 && search;j++)
  {
    n_j++;
    if(amp.at(j)-bck<(*max)-3*rms_bck || j-j_start>100)
    {
      search=false;
    }
  }
  
  double temp0;
  double temp1;
  double temp2;
  double temp3;
  double temp4;
  if(j_start==0){
    j_start=1;
  }
  if(j_start+n_j==camp-1){
    n_j=n_j-1;
  }

  TF1 f2b("f2b","[0]+(x-[1])*pow(x-[1],1)+[2]*pow(x-[1],2)",time.at(j_start-1),time.at(j_start+n_j+1));
  TF1 f4b("f4b","[0]+(x-[1])*pow(x-[1],1)+[2]*pow(x-[1],2)+[3]*pow(x-[1],3)+[4]*pow(x-[1],4)",time.at(j_start-1),time.at(j_start+n_j+1));
  f2b.SetParameters(amp.at(j_max),time.at(j_max),0,0);

  if(n_j<2)
    n_j=2;

  TGraph *g = new TGraph(n_j);
  for(int j=0;j<n_j;j++)
  {
    g->SetPoint(j,time.at(j+j_start),amp.at(j+j_start)-bck);
  }
  g->Fit("f2b","QN","",time.at(j_start),time.at(j_start+n_j));
  f4b.SetParameters(amp.at(j_max),time.at(j_max),f2b.GetParameter(2),0,0);
  g->Fit("f4b","QN","",time.at(j_start),time.at(j_start+n_j));

  *pamp = f4b.GetMaximum();
  *tamp = f4b.GetMaximumX();
  *chi = f4b.GetChisquare()/f4b.GetNDF();

  delete g;
  */
  return;
}

// Time at which a fixed threshold is passed
//(record lenght, amplitude vector, time vector, upper limit for background, upper limit for pulse, average background amplitude, threshold to measure time, time at which the threshold is passed)
void Thrtime(vector<double_t> time, vector<double_t> amp, Double_t t_bck, Double_t t_pul, Double_t bck,Double_t threshold[], Double_t thrtime[], int nth, Double_t max)
{
  int camp = time.size();
  double m;
  double q;
  bool search[100];
  std::fill_n(search, 100, true); 
  std::fill_n(thrtime, nth, -1); 
  for(int thindex=0;thindex<nth;thindex++){
    if(threshold[thindex]>max){
      search[thindex] = false;
    }
  }
  for(int j=1;j<camp;j++){
    if(time.at(j)<t_bck || time.at(j)>t_pul){
      continue;
    }
    for(int thindex=0;thindex<nth;thindex++){
      if(search[thindex]){
        if(time.at(j)>t_bck && time.at(j)<t_pul){
          if((amp.at(j)-bck-threshold[thindex])>0){
            m=((amp.at(j))-(amp.at(j-1)))/(time.at(j)-time.at(j-1));
            q=(amp.at(j)-bck)-m*time.at(j);
            thrtime[thindex]=(threshold[thindex]-q)/m;
            search[thindex]=false;
          }
        }
      }
    }
  }
  return;
}


// Time at which a fixed threshold is passed
//(record lenght, amplitude vector, time vector, upper limit for background, upper limit for pulse, average background amplitude, threshold to measure time, time at which the threshold is passed)
void ConstFracTime(vector<double_t> time, vector<double_t> amp, Double_t t_bck, Double_t t_pul, Double_t bck, Double_t max_time, Double_t threshold, Double_t *thrtime)
{
  int camp = time.size();
  *thrtime=-1;
  double m;
  double q;
  bool search=true;
  for(int j=1;j<camp && search;j++){
    if(time.at(j)>max_time && search){
      for(int k=1;k<j && search;k++){
        if((amp.at(j-k)-bck-threshold)<0 && (amp.at(j-k+1)-bck-threshold>0)){
          m=((amp.at(j-k))-(amp.at(j-k+1)))/(time.at(j-k)-time.at(j-k+1));
          q=(amp.at(j-k+1)-bck)-m*time.at(j-k+1);
          *thrtime=(threshold-q)/m;
          search=false;
        }
      }
    }
  }
  return;
}

// Time at which a fixed threshold is passed (descending)
//(record lenght, amplitude vector, time vector, upper limit for background, upper limit for pulse, average background amplitude, threshold to measure time, time at which the threshold has been passed, time of the trailing edge, isteresys with respect to the leading edge)
void Trailtime(vector<double_t> time, vector<double_t> amp,Double_t bck,Double_t threshold[], Double_t thrtime[], Double_t trailtime[], int nth, Double_t ister)
{
  int camp = time.size();
  bool search[100];
  std::fill_n(search, 100, true); 
  std::fill_n(trailtime, nth, 0); 

  for(int j=1;j<camp;j++){
    for(int thindex=0;thindex<nth;thindex++){
      if(search[thindex]){
        if(time.at(j)>thrtime[thindex]+ister){
          if((amp.at(j)-bck-threshold[thindex])<0){
            search[thindex] = false;
            trailtime[thindex]=(time.at(j)+time.at(j-1))/2.;
          }
        }
      }
    }
  }
  return;
}

// Generates FFT of a real vector (inreal) of lenght camp. Real part of output FFT is outre, immaginary part is outim. Module output is outmod.
void FFTrealtocomplex(vector<double_t> inreal, vector<double_t> *outre, vector<double_t> *outim, 
vector<double_t> *outmod)
{
  int camp = inreal.size();
  Double_t re;
  Double_t im;
  (*outre).clear();
  (*outim).clear();
  (*outmod).clear();
  TVirtualFFT *fftr2c = TVirtualFFT::FFT(1, &camp, "R2C");
  for(int i=0;i<camp;i++){
    fftr2c->SetPoint(i,inreal.at(i));
  }
  fftr2c->Transform();
  for (Int_t i=0; i<camp+1; i++){ // NOTE: the complex array has a size equal to input +1
    fftr2c->GetPointComplex(i, re, im);
    (*outre).push_back(re);
    (*outim).push_back(im);
    (*outmod).push_back(re*re+im*im);
  }
  fftr2c->Delete();
  
  return;
}

// Generates the inverse FFT from a complex vector of lenght camp (two vectors: inre real part, inim immaginary part). The output vector is outreal.
void FFTcomplextoreal(vector<double_t> inre, vector<double_t> inim, vector<double_t> *outreal)
{
  int camp = (*outreal).size();
  TVirtualFFT *fftc2r = TVirtualFFT::FFT(1, &camp, "C2R");
  for(int i=0;i<camp+1;i++){ // NOTE: the complex array has a size equal to input +1
    fftc2r->SetPoint(i,inre.at(i),inim.at(i));
  }  
  fftc2r->Transform();
  for (Int_t i=0; i<camp; i++)
  {
    (*outreal).at(i)=fftc2r->GetPointReal(i)/double(camp);
  }
  fftc2r->Delete();
  
  return;
}


void HighPass(vector<double_t> Freq, vector<double_t> *FFTre, vector<double_t> *FFTim, Double_t fcut){
  int camp = Freq.size();
  for(int i=0;i<camp;i++){
    if(Freq[i]<fcut){
      (*FFTre).at(i)=(Freq.at(i)/fcut)*(*FFTre).at(i);
      (*FFTim).at(i)=(Freq.at(i)/fcut)*(*FFTim).at(i);
      (*FFTre).at(2*camp-i-1)=(Freq.at(2*camp-i-1)/fcut)*(*FFTre).at(2*camp-i-1);
      (*FFTim).at(2*camp-i-1)= (Freq.at(2*camp-i-1)/fcut)*(*FFTim).at(2*camp-i-1);
    }
  }
  return;
}

// Waveform pre-processing 

// Computes the mobile average of the signal
// (record lenght, amplitude vector, time vector, number of points for the mobile average (odd number), averaged amplitude vector)
void mobileAVG(vector<double_t> amp, Int_t navg, vector<double_t> *m_amp)
{
  float tempsum=0;
  int camp = amp.size();
  for(int j=int((navg-1)/2);j<camp-int((navg-1)/2)-1;j++)
  {
    tempsum=0;
    for(int k=-int((navg-1)/2);k<int((navg-1)/2)+1;k++)
    {
      tempsum=tempsum+amp.at(j+k);
    }
    (*m_amp).at(j)=tempsum/float(navg);
  }
  for(int j=0;j<int((navg-1)/2);j++)
  {
    (*m_amp).at(j)=(*m_amp).at(int((navg-1)/2));
  }
  for(int j=camp-int((navg-1)/2)-1;j<camp;j++)
  {
    (*m_amp).at(j)=(*m_amp).at(camp-int((navg-1)/2)-2);
  }

  return;
}

// Computes the mobile average of the signal
// (record lenght, amplitude vector, time vector, number of points for the mobile average (odd number), averaged amplitude vector)
void expo_mobileAVG(vector<double_t> amp, Int_t ncut, Double_t alpha, vector<double_t> *m_amp)
{
  int camp = amp.size();
  double tempsum = 0;
  (*m_amp).at(0) = amp.at(0);
  for(int sample = 0; sample < ncut; sample++){ // First part, not approximated
    tempsum = 0;
    for(int subs = 0; subs<sample; subs++){
      tempsum = tempsum + alpha*pow(1-alpha,subs)*amp.at(sample-subs);
    }
    (*m_amp).at(sample) = tempsum;
  }
  for(int sample = ncut; sample< camp; sample++){ // Second part, approximated
    tempsum = 0;
    for(int subs = 0; subs<ncut; subs++){
      tempsum = tempsum + alpha*pow(1-alpha,subs)*amp.at(sample-subs);
    }
    (*m_amp).at(sample) = tempsum;
  }
  return;
}

//Inversione degli impulsi negativi
void inversion(vector<double_t> *m_amp)
{
  int camp = (*m_amp).size();
  for(int j=0;j<camp;j++)
  {
    (*m_amp).at(j)=-(*m_amp).at(j);
  }
}

void pulseskew(vector<double_t> amp, Int_t nskew, vector<double_t> *m_amp){
  int camp = amp.size();
  for(int j=0;j<nskew;j++){
    (*m_amp).at(j)=(*m_amp).at(nskew);
  }
  for(int j=nskew;j<camp;j++){
    (*m_amp).at(j)=amp.at(j)-amp.at(j-nskew);
  }
  return;
}

void waveoperation(vector<double_t> amp1, vector<double_t> amp2, vector<double_t> *m_amp, string op){
  int camp = amp1.size();
  if(op == "+"){
    for(int j=0;j<camp;j++){
      (*m_amp).push_back(amp1.at(j) + amp2.at(j));
    }
  }
  else if(op == "-"){
    for(int j=0;j<camp;j++){
      (*m_amp).push_back(amp1.at(j) - amp2.at(j));
    }
  }
  else{
    cerr << "Undefined operation between waveforms!" << endl;
    return;
  }
  return;
}


// DEPRECATED

//########################################################

// The following functions are deprecated. They will not be available in new versions of the software.

//########################################################

// Compute maximum, amplitude and peak time for waveform
//(record lenght, amplitude vector, time vector, upper limit for background, upper limit for pulse, average background amplitude, pulse maximum amplitude (averaged), pulse maximum amplitude)
void MaxFunc(Int_t camp, vector<double_t> time, vector<double_t> amp,  Double_t bck, Double_t t_bck,Double_t t_pul, Double_t *max,Double_t *tmax)
{
  int np=0;
  int cont=0;
  int j_max;
  int j_start;
  int n_j = 0;
  double tempmean;
  double tempsig;
  *max=-100000;
  //Maximum calculation
  for(int j=0;j<camp-1;j++)
  {
    if(time.at(j)>t_bck && time.at(j)<t_pul)
    {
      np++;
      if(amp.at(j)-bck>*max)
      {
        *max=amp.at(j)-bck;
        *tmax=(time.at(j+1)+time.at(j))/2.;
        j_max=j;
      }
    }
  }
  return;
}




// DEVELOPEMENT

//########################################################

// The following functions are not fully qualified. They should be used with caution.

//########################################################

// Shifts and substracts the waveform from itself
// (record lenght, amplitude vector, time vector, number of points for the mobile average (odd number), averaged amplitude vector)


// Integrates the pulse to give charge - readout impedance must be known
//(record lenght, amplitude vector, time vector, upper limit for background, upper limit for pulse, average background amplitude, pulse charge, total charge, readout resistance)
void Charges(vector<double_t> time, vector<double_t> amp, Double_t t_bck, Double_t t_pul, Double_t bck, Double_t *cha, Double_t *totcha, double R)
{
  int camp = time.size();
  double tempminch=0;
  double tempmaxch=0;
  double tempmintotch=0;
  double tempmaxtotch=0;
  for(int j=0;j<camp-1;j++)
  {
    if(time.at(j)>t_bck && time.at(j)<t_pul)
    {
      if(amp.at(j)>amp.at(j+1))
      {
        tempminch=tempminch+amp.at(j+1)-bck;
        tempmaxch=tempmaxch+amp.at(j)-bck;
      }
      if(amp.at(j)<=amp.at(j+1))
      {
        tempminch=tempminch+amp.at(j)-bck;
        tempmaxch=tempmaxch+amp.at(j+1)-bck;
      }
    }
    if(time.at(j)>t_bck)
    {
      if(amp.at(j)>amp.at(j+1))
      {
        tempmintotch=tempmintotch+amp.at(j+1)-bck;
        tempmaxtotch=tempmaxtotch+amp.at(j)-bck;
      }
      if(amp.at(j)<=amp.at(j+1))
      {
        tempmintotch=tempmintotch+amp.at(j)-bck;
        tempmaxtotch=tempmaxtotch+amp.at(j+1)-bck;
      }
    }
  }
  *cha=((tempmaxch+tempminch)/2.)*(time.at(1)-time.at(0))/R;
  *totcha=((tempmaxtotch+tempmintotch)/2.)*(time.at(1)-time.at(0))/R;
  return;
}


//(record lenght, amplitude vector, time vector, upper limit for background, upper limit for pulse, average background amplitude, threshold to comupute efficiency, efficiency)
void Efficiencies(Int_t camp, vector<double_t> time, vector<double_t> amp, Double_t t_bck, Double_t t_pul, Double_t bck,Double_t threshold, Double_t *eff)
{
  *eff=0;
  for(int j=0;j<camp;j++)
  {
    if(time.at(j)<t_bck)
      continue;
    if(time.at(j)>t_bck && time.at(j)<t_pul)
    {
      if((amp.at(j)-bck-threshold)>0 && threshold>0)
      {
        *eff=1;
        break;
      }
      if((amp.at(j)-bck-threshold)<0 && threshold<0)
      {
        *eff=1;
        break;
      }
    }
  }
  return;
}

// Time at which a (low) fixed threshold is passed. Requires confirmation with a larger fixed trhreshold.
//(record lenght, amplitude vector, time vector, upper limit for background, upper limit for pulse, average background amplitude, threshold to measure time, confirmation threshold, time at which the threshold is passed)
void Doublethrtime(Int_t camp, vector<double_t> time, vector<double_t> amp, Double_t t_bck, Double_t t_pul, Double_t bck,Double_t threshold1, Double_t threshold2, Double_t *thrtime)
{
  *thrtime=0;
  double m;
  double q;
  bool arm=false;
  bool shot=false;
  for(int j=0;j<camp;j++)
  {
    if(time.at(j)<t_bck)
      continue;
    if(time.at(j)>t_pul)
      continue;
    if(time.at(j)>t_bck && time.at(j)<t_pul)
    {
      if((amp.at(j)-bck-threshold1)<0 && threshold1>0 && arm && !shot)
      {
	arm=false;
      }
      if((amp.at(j)-bck-threshold1)>0 && threshold1>0 && !arm && !shot)
      {
        m=((amp.at(j))-(amp.at(j-1)))/(time.at(j)-time.at(j-1));
        q=(amp.at(j)-bck)-m*time.at(j);
        *thrtime=(threshold1-q)/m;
	arm=true;
      }
      if((amp.at(j)-bck-threshold2)>0 && threshold2>0 && arm && !shot)
      {
	shot=true;
        break;
      }
    }
  }
  return;
}

// Says if the event suffered from saturation of the waveform readout or not. Must be calibrated each time to evaluate the sensitivity of the routine
//(record lenght, amplitude vector, time vector,saturation flag)
void Saturation(Int_t camp, vector<double_t> time, vector<double_t> amp, Double_t bck, Int_t *satur)
{
  *satur=0;
  int cont=0;
  int tempcont=0;
  double value=100000;
  for(int j=1;j<camp;j++)
  {
    if(fabs(amp.at(j)-amp.at(j-1))>0.0001)
    {
      tempcont=0;
    }
    if(fabs(amp.at(j)-amp.at(j-1))<0.0001)
    {
      tempcont++;
      if(tempcont>cont)
      {
        cont=tempcont;
        value=amp.at(j);
      }
    }
  }
  if(fabs(value-bck)>1. && cont>int(camp/500)+5)
    *satur=1;
  return;
}

// Computed the charge centroid using the A0/cosh((x-x0)/delta0) function.
//(Charge, Error of charge, number of RO channels, distribution width, distribution width 2nd method, Distribution const, Distribution const 2nd method, Centroid , Centroid 2nd method , ChiSquare, ChiSquare 2nd method,pitch, Cluster size, threshold)
void Centroid(Double_t ch[], Double_t cherr[],Int_t nchro, Double_t *delta0, Double_t *delta1, Double_t *A0, Double_t *A1, Double_t *x0, Double_t *x1, Double_t *chinorm0, Double_t *chinorm1, Double_t pitch, Int_t CS, Double_t threshold)
{
  TF1 cint("cint",Form("[0]*atan(exp((x+%f-[1])/[2]))-[0]*atan(exp((x-%f-[1])/[2]))",pitch/2.,pitch/2.),-100.,100.);
  TF1 cdis("cdis","[0]*(1./cosh((x-[1])/[2]))",-100.,100.);
  TF1 cintf("cintf",Form("[0]*atan(exp((x+%f-[1])/[2]))-[0]*atan(exp((x-%f-[1])/[2]))",pitch/2.,pitch/2.),-100.,100.);
  TGraphErrors gbar(CS);
  double tmpmax=-100;
  double tmpbar=0;
  int k=0;
  *delta0=-1;
  *delta1=-1;
  *A0=-1;
  *A1=-1;
  *x0=-10;
  *x1=-10;
  *chinorm0=-1;
  *chinorm1=-1;
  cintf.FixParameter(2,3.9);
  if(CS>2)
  {
    for(int i=0;i<nchro;i++)
    {
      if(ch[i]>tmpmax)
      {
        tmpmax=ch[i];
        tmpbar=double(i)*pitch+pitch/2.;
      }
    }

    cint.SetParameters(tmpmax,tmpbar,3.5);
    cint.SetParLimits(1,-pitch*5.,double(nchro)*pitch+pitch*5.);
    cintf.SetParameters(tmpmax,tmpbar,3.9);
    cintf.FixParameter(2,3.9);
    cintf.SetParLimits(1,-pitch*5.,double(nchro)*pitch+pitch*5.);

    for(int i=0;i<nchro;i++)
    {
      if(ch[i]>threshold)
      {
        k++;
        gbar.SetPoint(k,double(i)*pitch+pitch/2.,ch[i]);
        gbar.SetPointError(k,0.,cherr[i]);
      }
    }

    gbar.Fit("cint","QN","",0.,nchro*pitch+pitch/2.);
    *delta0=cint.GetParameter(2);
    *x0=cint.GetParameter(1);
    *A0=cint.GetParameter(0);
    *chinorm0=cint.GetChisquare()/cint.GetNDF();
    gbar.Fit("cintf","QN","",0.,nchro*pitch+pitch/2.);
    *delta1=cintf.GetParameter(2);
    *x1=cintf.GetParameter(1);
    *A1=cintf.GetParameter(0);
    *chinorm1=cintf.GetChisquare()/cintf.GetNDF();
  }
  if(CS==2)
  {
    *x0=0;
    float tempnum=0;
    float tempden=0;
    float bar0temp=0;
    float tempor=0;
    for(int j=0;j<nchro;j++)
    {
      if(ch[j]>threshold)
      {
        bar0temp=bar0temp+(float(j)*pitch+pitch/2.);
      }
    }
    bar0temp=bar0temp/2.;
    for(int j=0;j<nchro;j++)
    {
      if(ch[j]>threshold)
      {
        tempnum=tempnum+ch[j]*(float(j)*pitch+pitch/2.-bar0temp);
        tempden=tempden+ch[j];
        //cout << FEcharge[j] << "\t";
      }
    }
    tempor=tempnum/tempden;
    *x0=bar0temp+7.5*tempor;  //7.5 conversion factor from charge centroid to mm
  }
  if(CS==1)
  {
    *x0=0;
    for(int j=0;j<nchro;j++)
    {
      if(ch[j]>threshold)
      {
        *x0=*x0+(float(j)*pitch+pitch/2.);
      }
    }
  }
  return;
}

// Subtracts the sinusoidal noise from the waverform using a fit around the pulse
void NoiseClean(Int_t camp, vector<double_t> time, vector<double_t> amp,  Double_t t_bck, Double_t t_pul, vector<double_t> m_amp)
{
  TF1 f("fsin","[0]+[1]*sin([2]*x+[3])",t_bck-12,t_pul+12);
  TGraphErrors g(camp);
  int gp=0;
  double max=0;
  for(int i=0;i<camp;i++){
    if((time.at(i)>t_bck-10 && time.at(i)<t_bck) || (time.at(i)>t_pul && time.at(i)<t_pul+10)){
      gp++;
      g.SetPoint(gp,time.at(i),amp.at(i));
      g.SetPointError(gp,0.0001,0.1);
      if(amp.at(i)>max){
	      max=amp.at(i);
      }
    }
  }
  f.SetParameters(0,max,1.4,0);
  g.Fit("fsin","QNM","",t_bck-10,t_pul+10);
  for(int i=0;i<camp;i++){
    m_amp.at(i)=m_amp.at(i)-f(time.at(i));
  }
  return;
}

//Reshaping
// Calculates the 4-point derivative of the signal
// (record lenght, amplitude vector,reshaping vector)
void Reshaping(Int_t camp, vector<double_t> amp,vector<double_t> resh)
{
  resh.push_back(0);
  resh.push_back(0);
  for(int j=2;j<camp-2;j++)
  {
    resh.push_back((1./12.)*(amp.at(j-2)-8.*amp.at(j-1)+8.*amp.at(j+1)-amp.at(j+2)));
  }
  resh[0]=resh[2];
  resh[1]=resh[2];
  resh.push_back(resh[camp-3]);
  resh.push_back(resh[camp-3]);
  return;
}

// Polinomial correction of coordinates for centroid estimation (expansion at center of strips and compression at borders)
//(centroid variable address, curve parameter)
void positionbias(Double_t *bar,Double_t sca)
{
  float barfz=*bar;
  float xloop=(1/100000.)*(int((100000.)*(barfz-4))%800000);
  float halfloop=(1/100000.)*(int(100000.*barfz)%400000);

  //float pi=4.*atan(1.);

  // *bar=(4./(-acos(4.*sca)+pi/2.))*(-acos(sca*((1./100000.)*((*bar)-4))%800000)-4))+pi/2.)+((*bar)-(1./100000.)*((*bar)-4))%800000)-4.)+8.;



  //*bar=(1/(sca+16.))*((sca)*((1/100000.)*(int((100000.)*((barfz)-4))%800000)-4)+pow(((1/100000.)*(int((100000.)*((barfz)-4))%800000)-4),3))+((barfz)-(1/100000.)*(int((100000.)*(barfz-4))%800000)-4)+8;

  *bar=((1/(70.+16.))*((70.)*(xloop-4)+pow((xloop-4),3))+(barfz-xloop-4)+8);

  return;
}

//Function to fit the waveform with Gumbel function (check for possible memory leaks)
void Gumbel_fit(Int_t camp, vector<double_t> time, vector<double_t> amp,Double_t bck,Double_t t_low,Double_t t_up,Double_t t_amp, Double_t t_width, Double_t max, Double_t *bck_fit, Double_t *t_amp_fit, Double_t *t_width_fit, Double_t *amp_fit)
{
  TF1 *fgum = new TF1("gumbel","[0]+[3]*(exp(-(x-[1])/[2]+exp(-(x-[1])/[2])))",t_low,t_up);
  fgum->SetParameters(bck,t_amp,t_width,max);

  TGraph *g = new TGraph();
  int npoints=0;
  for(int i=0;i<camp;i++)
  {
    if(time.at(i)<t_low)
      continue;
    if(time.at(i)>t_up)
      break;
    npoints++;
  }
  g->Set(npoints);
  npoints=0;
  for(int i=0;i<camp;i++)
  {
    if(time.at(i)<t_low)
      continue;
    if(time.at(i)>t_up)
      break;
    g->SetPoint(npoints,time.at(i),amp.at(i));
    npoints++;
  }

  g->Draw();
  g->Fit("gumbel","QN","goff",t_low,t_up);
  *bck_fit=fgum->GetParameter(0);
  *t_amp_fit=fgum->GetParameter(1);
  *t_width_fit=fgum->GetParameter(2);
  *amp_fit=fgum->GetParameter(3);

  g->Delete();
  fgum->Delete();

  return;
}

//Function to fit rise time of the waveform with polinomial function // NOTE: parameter preset is required before calling the fuction!
void RiseFit(Int_t camp, vector<double_t> time, vector<double_t> amp,Double_t bck,Double_t t_low,Double_t t_up, Double_t *fit_par0, Double_t *fit_par1, Double_t *fit_par2, Double_t *fit_par3, Double_t *fit_par4,Double_t *time_flex)
{
  TF1 *fpol4 = new TF1("fpol4","[0]+[1]*x+[2]*x*x+[3]*x*x*x+[4]*x*x*x*x",t_low,t_up);

  double a,b,c;

  fpol4->SetParameters(-25197.5,4133.39,-253,6.9,-0.07);

  TGraph *g = new TGraph();
  int npoints=0;
  for(int i=0;i<camp;i++)
  {
    if(time.at(i)<t_low)
      continue;
    if(time.at(i)>t_up)
      break;
    npoints++;
  }
  g->Set(npoints);
  npoints=0;
  for(int i=0;i<camp;i++)
  {
    if(time.at(i)<t_low)
      continue;
    if(time.at(i)>t_up)
      break;
    g->SetPoint(npoints,time.at(i),amp.at(i));
    npoints++;
  }

  g->Draw();
  g->Fit("fpol4","QN","goff",t_low,t_up);
  *fit_par0=fpol4->GetParameter(0);
  *fit_par1=fpol4->GetParameter(1);
  *fit_par2=fpol4->GetParameter(2);
  *fit_par3=fpol4->GetParameter(3);
  *fit_par4=fpol4->GetParameter(4);

  a=*fit_par4*4.*3.;
  b=*fit_par3*3.*2.;
  c=*fit_par2*2.;

  if((-b-sqrt(b*b-4.*a*c))/(2.*a)>t_low && (-b-sqrt(b*b-4.*a*c))/(2.*a)<t_up)
    *time_flex=(-b-sqrt(b*b-4.*a*c))/(2.*a);
  else if((-b-+sqrt(b*b-4.*a*c))/(2.*a)>t_low && (-b+sqrt(b*b-4.*a*c))/(2.*a)<t_up)
    *time_flex=(-b+sqrt(b*b-4.*a*c))/(2.*a);
  else
    *time_flex=-999;
  g->Delete();
  fpol4->Delete();

  return;
}


#endif
