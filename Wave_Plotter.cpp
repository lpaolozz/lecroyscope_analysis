// ROOT includes
#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TF1.h"
#include "TString.h"
#include "TMath.h"
#include "TVirtualFFT.h"
#include "TApplication.h"
#include "TCanvas.h"
#include "TSystem.h"

// Standard includes
#include <vector>
#include <string>
#include <fstream>
#include <iomanip>
#include <sys/stat.h>
#include <sstream>
#include <stdlib.h>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <dirent.h>
#include <algorithm>
#ifndef __has_include
  static_assert(false, "__has_include not supported");
  #else
  #  if __cplusplus >= 201703L && __has_include(<filesystem>)
  #    include <filesystem>
       namespace fs = std::filesystem;
  #  elif __has_include(<experimental/filesystem>)
  #    include <experimental/filesystem>
       namespace fs = std::experimental::filesystem;
  #  elif __has_include(<boost/filesystem.hpp>)
  #    include <boost/filesystem.hpp>
       namespace fs = boost::filesystem;
  #  endif
#endif


// Lorenzo includes
#include "Setup.h"
#include "General_Functions.h"
#include "Conversion.h"
#include "Wave_Analyzer.h"


using namespace std;
//namespace fs = std::filesystem;

int main(int argc, char** argv){  
  ifstream InputCARD;
  ifstream runlist;
  ifstream runlist_temp_check;
  bool list = false;
  bool corrupted = false;
  
  if(argc>=2){
    if(fs::exists(argv[1]) && fs::is_directory(argv[1])){
      cout << "Converting data in folder: " << argv[1] << endl;
      list = false;
    }
    else{
      std::cerr << "Usage: " << argv[0] << " path_to_data_folder" << std::endl;
	    exit(EXIT_FAILURE);
    }
  }
  else{
    std::cerr << "Usage: " << argv[0] << " path_to_data_folder" << std::endl;
    exit(EXIT_FAILURE);
  }

  if(argc==3){
    if(string(argv[2]).compare(0,2,"-c")==0){
      corrupted = true;
      cout << " ####################################### " << endl << endl;
      cout << " Running corrupted data recovery routine" << endl << endl;
      cout << " ####################################### " << endl;
    }
  }

  //#######################   DATA I/O AND PARAMETER SETUP  #######################

  // Default parameters

  bool doFFT=false;  
  bool showFFT=false;
  bool domobileavg=false;
  bool showwave_select=false;
  bool printheader=false;
  bool stdvariables=true;
  int campfact=1;  //divider to sample rate for waveform memorization
  int maxtrig=999999;
  int nchro=4;
  Double_t t_bck[4]={10,10,10,10};
  Double_t t_pul[4]={500,500,500,500};
  Double_t thr[4]={20,20,20,20};
  int polarity[4]={1,1,1,1};
  

  // Program variables
  int nrecord_wave = 5;
  string selection="";
  bool newcard=false;
  string pip;
  bool running=true;
  string inpath[1000];
  string outpath[1000];
  string firstseg[1000];
  string lastseg[1000];
  int nruns=0;
  vector<string> runmatrix[8]; //temporary matrix where the names of the data files are stored
  int runindex[8];
  double fcuthigh[8]={1e9,1e9,1e9,1e9,1e9,1e9,1e9,1e9}; //expressed in Hertz
  double fcutlow[8]={0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05}; //expressed in GHertz
  double SR; // sampling rate (in Gs/s) extracted from waveform
  Double_t doublebuffer;

  // Opening Input parameter CARD
  cout << endl;
  InputCARD.open("./InputCARD.in");
  double tmin=0;
  double tmax=1e12;

  cout << "Do you want to do a mobile average (y/N)?" << endl;
  getline(std::cin, selection, '\n' );
  if(selection=="y" || selection=="yes" || selection=="Y"){
    domobileavg=true;
  }
  cout << "Do you want to do an FFT (y/N)?" << endl;
  getline(std::cin, selection, '\n' );
  if(selection=="y" || selection=="yes" || selection=="Y"){
    doFFT=true;
  }
  if(domobileavg){
    for(int ch=0;ch<nchro;ch++){
      cout << endl << "Enter mobile average lowpass cutoff frequency (in GHz) for channel " << ch+1 << endl;
      cin >> fcuthigh[ch];
      fcuthigh[ch]=fcuthigh[ch]*1e9;
    }
  }
  if(doFFT){
    for(int ch=0;ch<nchro;ch++){
      cout << endl << "Enter highpass cutoff frequency (in GHz) for channel " << ch+1 << endl;
      cin >> fcutlow[ch];
      fcutlow[ch]=fcutlow[ch];
    }
  }

  cout << "Set lower time for visualization in ns" << endl;
  cin >> tmin;
  
  cout << "Set upper time for visualization in ns" << endl;
  cin >> tmax;

  // Reading RunList
  if(list){
    ReadList( &runlist, &inpath[0], &outpath[0],&firstseg[0], &lastseg[0], &nruns);
    cout << endl << nruns << " runs will be analyzed." << endl;
  }
  else{
    nruns = 1;
    inpath[0] = argv[1];
    string tempstr = argv[1];
    if(tempstr.compare(tempstr.length()-1,1,"/") == 0){
      outpath[0] = tempstr.substr(0,tempstr.length()-1) + ".root";
    }
    else{
      outpath[0] = tempstr.substr(0,tempstr.length()) + ".root";
    }
    cout << "Outputfile: " << outpath[0] << endl;
  }
  TApplication App("app", &argc, argv);
  selection="";

  //#######################   RUN LOOP  #######################

  for(int n=0;n<nruns;n++){

    //#######################   VARIABLE AND OUTPUT FILE DEFINITION  #######################

    
    uint nfirstseg = 0;
    uint nlastseg = 1000000;

    if(is_number(firstseg[n]) || is_number(lastseg[n])){
      cout << "Selected segment range for run " << n+1 << ":" << endl;
    }
    if(is_number(firstseg[n])){
      nfirstseg=stol(firstseg[n]);
      cout << "First Segment = " << nfirstseg << endl;
    }
    if(is_number(lastseg[n])){
      nlastseg=stol(lastseg[n]);
      cout << "Last Segment = " << nlastseg << endl;
    }
    if(nfirstseg>nlastseg){
      cerr << "ERROR: first segment should be smaller than last segment." << endl;
      return(EXIT_FAILURE);
    }

    for(int i=0;i<8;i++){
      runmatrix[i].clear();
    }

    // Building runmatrix from runlist
    int matx=RunMatrix( n, nfirstseg, nlastseg, &runindex[0], runmatrix, &inpath[0], &outpath[0]);
    if(matx==0){
      return(EXIT_FAILURE);
    }

    
    int i;
    int j;
    int k;

    // Scan variables
    Int_t nrun = n+1; // Run position in the RunList
    Int_t ntrig = 0; // Event number in the run
    Int_t nseg = 0; // Segment number
    int SegTrig = 0; // Counter of triggers inside a segment


    
    // Waveform variables
    bool showwave = true;
    Double_t year; // (6 variables) Waveform acquisition date
    Double_t month; 
    Double_t day; 
    Double_t hour;
    Double_t mnt;
    Double_t sec;
    double TrigTime; // Absolute trigger time
    Int_t chan[nchro]; // Channel identifier
    Int_t Nbck[nchro]; // Number of background samples
    Double_t bck[nchro]; // Average background value
    Double_t max_bck[nchro]; // Maximum background value
    Double_t rms_bck[nchro]; // RMS of background

    Int_t camp[nchro]; // Number of waveform samples
    Int_t campview[nchro]; // Number of samples for recorded waveform in the output file

    const Int_t max_camp = 48100; // Maximum number of samples allowed for the waveform
    const Int_t max_FFT_camp = 48100; // Maximum number of samples allowed for the FFT
    const Int_t max_camprec = int(max_camp/campfact)+100; // Maximum number of samples allowed for the waveform in the output file
    vector<double_t> time[nchro]; // Time vectors
    vector<double_t> amp[nchro]; // Amplitude vectors
    vector<double_t> m_amp[nchro]; // Processed amplitude vectors

    vector<double_t> timerec[nchro]; // Time vectors (recorded waveform)
    vector<double_t> amprec[nchro]; // Amplitude vectors (recorded waveform)
    vector<double_t> m_amprec[nchro]; // Processed amplitude vectors (recorded waveform)
    
    Double_t max[nchro]; // Pulse maximum
    Double_t amplitude[nchro]; // Pulse amplitude
    Double_t min[nchro]; // Pulse lowest point
    Double_t max_time[nchro]; // Pulse peaking time
    Double_t amp_time[nchro]; // Pulse peaking time
    Double_t time_10const[nchro]; // Time at 10% of the amplitude
    Double_t time_20const[nchro]; // Time at 20% of the amplitude
    Double_t time_50const[nchro]; // Time at 50% of the amplitude
    Double_t time_80const[nchro]; // Time at 80% of the amplitude
    Double_t time_90const[nchro]; // Time at 90% of the amplitude
    Double_t time_3rms[nchro]; // Rising edge time at 3rms of the background
    Double_t time_5rms[nchro]; // Rising edge time at 5rms of the background
    Double_t time_1thr[nchro]; // Rising edge time at 1mV fixed threshold
    Double_t time_2thr[nchro]; // Rising edge time at 2mV fixed threshold
    Double_t time_3thr[nchro]; // Rising edge time at 3mV fixed threshold
    Double_t time_4thr[nchro]; // Rising edge time at 4mV fixed threshold
    Double_t time_5thr[nchro]; // Rising edge time at 5mV fixed threshold
    Double_t time_7thr[nchro]; // Rising edge time at 7mV fixed threshold
    Double_t time_10thr[nchro]; // Rising edge time at 10mV fixed threshold
    Double_t time_20thr[nchro]; // Rising edge time at 20mV fixed threshold
    Double_t time_50thr[nchro]; // Rising edge time at 50mV fixed threshold
    Double_t time_100thr[nchro]; // Rising edge time at 100mV fixed threshold
    Double_t time_200thr[nchro]; // Rising edge time at 200mV fixed threshold
    Double_t time_thr[nchro]; // Rising edge time at custom fixed threshold
    Double_t trail_1thr[nchro]; // Falling edge time at 1mV fixed threshold
    Double_t trail_2thr[nchro]; // Falling edge time at 2mV fixed threshold
    Double_t trail_3thr[nchro]; // Falling edge time at 3mV fixed threshold
    Double_t trail_4thr[nchro]; // Falling edge time at 4mV fixed threshold
    Double_t trail_5thr[nchro]; // Falling edge time at 5mV fixed threshold
    Double_t trail_7thr[nchro]; // Falling edge time at 7mV fixed threshold
    Double_t trail_10thr[nchro]; // Falling edge time at 10mV fixed threshold
    Double_t trail_20thr[nchro]; // Falling edge time at 20mV fixed threshold 
    Double_t trail_50thr[nchro]; // Falling edge time at 50mV fixed threshold 
    Double_t trail_100thr[nchro]; // Falling edge time at 100mV fixed threshold
    Double_t trail_200thr[nchro]; // Falling edge time at 200mV fixed threshold
    Double_t trail_thr[nchro]; // Falling edge time at custom fixed threshold
    Double_t toa[nchro]; // Signal Time of Arrival at custom fixed threshold
    Double_t tot[nchro]; // Signal Time over Threshold at custom fixed threshold
    Double_t pulsecharge[nchro]; // Charge in the pulse window

    Double_t freq[nchro][max_FFT_camp]; // Frequency vector
    Double_t FFT_real[nchro][max_FFT_camp]; // Real component of the FFT
    Double_t FFT_imag[nchro][max_FFT_camp]; // Imaginary component of the FFT
    Double_t FFT_abs[nchro][max_FFT_camp]; // Magnitude of the FFT
  
    
  
    // Program variables
    string datafile;
    int emptycount=0;
    bool bad=false;
    bool header_printed=false;
    int arrayoff;
    uint datapos;
    uint channelpos;
    int SegCount = 0;
    Double_t chi2;
    bool setns=true;
    Int_t ns=1;
        
    //Inizialization of data run variables
    ntrig=0;
    nseg=0;

    //Inizialization of program run variables
    bool active_run=true;
    pip = "";
    
    //Inizialization of channel variables for this run
    camp[i]=0;
    campview[i]=0;

    cout << endl << endl << "############################################### " << endl << endl;
    cout << "Run number: " << nrun << endl;
    cout << "Number of Segments = " << runindex[0] << endl;
    cout << "Run data path: " << inpath[n] << endl;
    
    for(i=0;i<nchro;i++){
      time[i].clear();
      amp[i].clear();
      m_amp[i].clear();
      timerec[i].clear();
      amprec[i].clear();
      m_amprec[i].clear();
    }

    //Loop on different triggers of the run
    cout << "\nEvent number: " << flush;
    ifstream data[8];
    bool showed = true;

    TCanvas *Ccheck = new TCanvas("Ccheck", "Ccheck",20,71,1550,875); // Waveform
    Ccheck->Divide(2,2);
    Ccheck->Show();
    TGraph gplot[4];
    TGraph m_gplot[4];
    bool headerread=false;
    int byteoff = 0;
    int HeaderLength = 0;
    int FirstValidPoint = 0;
    float HorizontalInterval = 0;
    float VerticalGain = 0;
    float VerticalOffset = 0;
    int dataformat;
    while(active_run){
      int eventshow;
      if(showed){
        ntrig=0;
        SegTrig=0;
        SegCount=0;
        nseg=0;
        headerread=false;
        cout << "Insert event number (ntrig). Insert 0 to exit. Insert -1 to access the editor for the current Canvas." << endl;
        cin >> eventshow;
        showed=false;
        if(eventshow==0){
          cout << "Invalid selection or exit condition." << endl;
          return(EXIT_SUCCESS);
        }
        if(eventshow==-1){
          App.Run();
        }
        cout << "Showing event " << eventshow << endl;
      }
      ntrig++;
      SegTrig++;
      // Check if the segment is finished
      if(SegTrig==SegCount+1){
        nseg++;
        SegTrig=1;
        for(i=0;i<nchro;i++){
	        //#######################   OPENING DATA FILES  #######################
	        chan[i] = (Int_t)i;
	        datafile="";
	        datafile=runmatrix[i].at(nseg-1);  //Read file containing the event on that channel
          if(data[i].is_open()){
            data[i].close();
          }
	        data[i].open(datafile.c_str(), ios::binary|ios::ate);
	        if(!data[i]){            
	          active_run=false;
	          cerr << "\nData file not found! Run " << nrun << "  Event " << ntrig << endl << flush;
	          cout << i << "\t" << nseg << endl;
	          cout << runmatrix[i].at(nseg-2) << endl;
	          cout << "End of run" << endl << flush;
	          return(EXIT_FAILURE);
	        }
	      }
      }
      // Check if the run is finished
      if(nseg == runindex[0] && SegTrig==SegCount){
        active_run = false;
      }
      if(ntrig==maxtrig){
	      cout << "Maximum number of events reached: " << maxtrig << endl << flush;
	      active_run=false;
      }
      if(ntrig==1 || ntrig%200==0){
      	//cout << ntrig << ", " << flush;
      }
      

      //#######################   LOOP ON CHANNELS  #######################
      for(i=0;i<nchro;i++){

        
        chan[i] = (Int_t)i;
        setns=true;
        // Reading the header
        if(SegTrig==1){
          HeaderRO(&data[i], printheader, SegTrig, max_camp, &SegCount, &active_run, &bad, &datapos, &header_printed, &camp[i], &FirstValidPoint, &HorizontalInterval, &VerticalGain, &VerticalOffset, &dataformat, &day, &month, &year, &hour, &mnt, &sec, &TrigTime, false);
        }
        uint datapos_bck = datapos;
        if(ntrig==eventshow){
          HeaderRO(&data[i], printheader, SegTrig, max_camp, &SegCount, &active_run, &bad, &datapos, &header_printed, &camp[i], &FirstValidPoint, &HorizontalInterval, &VerticalGain, &VerticalOffset, &dataformat, &day, &month, &year, &hour, &mnt, &sec, &TrigTime, false);
          datapos = datapos_bck;
        }
        channelpos = datapos+((dataformat)+1)*((SegTrig)-1)*(camp[i]);

        
        while(time[i].size()<=camp[i]){
          time[i].push_back(0);
        }
        while(amp[i].size()<=camp[i]){
          amp[i].push_back(0);
        }
        while(m_amp[i].size()<=camp[i]){
          m_amp[i].push_back(0);
        }

        if(datapos<0 || channelpos<0){
          cerr << "ERROR: invalid position on file readout." << endl;
          return(EXIT_FAILURE);
        }
        // reading waveform      
        //cout << "Reading waveform" << endl;
        int maxamp=-1000000;
        int minamp=1000000;
        double bck2[4];
        if(ntrig == eventshow){
          EventRO(&data[i], channelpos, camp[i], campfact, time[i], amp[i], m_amp[i], polarity[i], dataformat, FirstValidPoint, HorizontalInterval, VerticalGain, VerticalOffset);
          Background(camp[i], time[i], m_amp[i], time[i][ns], time[i].at(camp[i]/5+ns), t_pul[i], &bck[i], &max_bck[i], &rms_bck[i]);
          for(int cmp=0;cmp<camp[i];cmp++){
            if(amp[i].at(cmp)>maxamp){
              maxamp=amp[i].at(cmp);
            }
            if(amp[i].at(cmp)<minamp){
              minamp=amp[i].at(cmp);
            }
          }
        }
        
        //cout << "Waveform read" << endl;


	      // Swap corrupted routine
	      if(corrupted && ntrig==eventshow){
	        vector<Double_t> amp_temp[camp[i]];
	        for(int tmp_count = 0;tmp_count<camp[i];tmp_count++){
	          amp_temp[tmp_count]=m_amp[tmp_count];
	        }
	        for(int tmp_count = 1;tmp_count<camp[i]-1;tmp_count++){
	          if(tmp_count<camp[i]/2){
	            m_amp[tmp_count]=amp_temp[tmp_count+camp[i]/2];
	          }
	          else{
	            m_amp[tmp_count]=amp_temp[tmp_count-camp[i]/2];
	          }
	        }
	      }

        //#######################   WAVEFORM ANALYSIS  #######################

	      // Operations on waveform (frequency domain)
        if(doFFT && ntrig==eventshow){          
          // all-range FFT
          for(int l=0; l<camp[i]; l++){
            freq[i][l] = double(l)/(HorizontalInterval*camp[i])*1e-9;
          }
          // Generating FFT
          FFTrealtocomplex(camp[i], amp[i], FFT_real[i], FFT_imag[i], FFT_abs[i]);

          // Operations on the FFT (filters) should be placed here
          // ### FFT OPERATIONS ####
          
          HighPass(camp[i], freq[i], FFT_real[i],FFT_imag[i],fcutlow[i]);

          // Regenerating waveform from the FFT
          FFTcomplextoreal(camp[i], FFT_real[i], FFT_imag[i], m_amp[i]);
        }

	      
        // Operations on waveform (time domain)       
        if(domobileavg && ntrig==eventshow){
          while(setns){
            SR=(1.e9/(time[i][1]-time[i][0]));
            double Hf=(1./double(ns))*fabs(sin(3.14*fcuthigh[i]*double(ns)/SR)/sin(3.14*fcuthigh[i]/SR));
            if(Hf<0.87){
              setns=false;
              cout << "Doing mobile average with " << ns << " poins, at a sampling rate of " << (SR*1e-9) << " GS/s." << endl;
              cout << "Attenutation at cutoff frequency:" << Hf << endl;
            }
            else{
              ns=ns+2;
            }
            if(ns>camp[i]/10){
              cout << endl << endl << "WARNING: The cutoff frequency is too low. No mobile average will be applied." << endl << endl;
              ns=1;
              setns=false;
            }
          }
          if(ns>1){
            mobileAVG(camp[i], m_amp[i], ns, &m_amp[i]);
          }
          
        }
        if(ntrig==eventshow){
          Background(camp[i], time[i], m_amp[i], time[i][ns], time[i].at(camp[i]/5+ns), t_pul[i], &bck2[i], &max_bck[i], &rms_bck[i]);
          campview[i]=0;
          for(int sample=0;sample<camp[i];sample++){
            if(time[i].at(sample)>tmin && time[i].at(sample)<tmax){
              campview[i]++;
            }
          }
          gplot[i].Set(campview[i]);
          if(domobileavg || doFFT){
            m_gplot[i].Set(campview[i]);
            m_gplot[i].SetLineColor(2);
            m_gplot[i].SetMarkerColor(2);
          }
          uint tempsample=0;
          for(int sample=0;sample<camp[i];sample++){
            if(time[i].at(sample)>tmin && time[i].at(sample)<tmax){
              gplot[i].SetPoint(tempsample,time[i].at(sample),amp[i].at(sample)-bck[i]);
              if(domobileavg || doFFT){
                m_gplot[i].SetPoint(tempsample,time[i].at(sample),m_amp[i].at(sample)-bck2[i]);
              }
              tempsample++;
            }
          }
          Ccheck->cd(i+1);
          gplot[i].Draw("AL");
          if(domobileavg || doFFT){
            m_gplot[i].Draw("LS");
          }
          Ccheck->Update();
          if(i==nchro-1){
            //App.Run();
          }
          showed = true;
        }       
      } // Loop on channels ends
      gSystem->ProcessEvents();
      
    } // Run loop ends here
  } // Scan loop ends here
  return(EXIT_SUCCESS);
}
